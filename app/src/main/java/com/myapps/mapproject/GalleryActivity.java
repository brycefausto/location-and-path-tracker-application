package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.media.ExifInterface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class GalleryActivity extends AppCompatActivity implements GridView.OnItemClickListener {

    public static final String[] EXTENSIONS = {
            "jpg", "jpeg", "png", "bmp", "gif"
    };
    public static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {
        @Override
        public boolean accept(File file, String s) {
            for (String ext : EXTENSIONS) {
                if (s.endsWith("." + ext)) {
                    return true;
                }
            }
            return false;
        }
    };
    private final Activity activity = this;
    private List<Picture> pictures = new ArrayList<>();
    private List<String> pictureNames = new ArrayList<>();
    private PictureAdapter adapter;
    private PictureViewAdapter viewAdapter;
    private GridView gridView;
    private ViewPager viewPager;
    private BottomSheetBehavior bottomSheetBehavior;
    private ImageButton btnLocation;
    private Picture selPicture;
    private boolean isViewPagerVisible;
    private String selImageName;
    private Intent intent;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gridView = findViewById(R.id.gridView);
        adapter = new PictureAdapter(this, pictures);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);
        viewPager = findViewById(R.id.viewPager);
        viewAdapter = new PictureViewAdapter(this, pictures);
        viewPager.setAdapter(viewAdapter);
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (positionOffset >= 0.5) {
                    View view = viewPager.findViewWithTag("view" + position);
                    TouchImageView imageView = view.findViewById(R.id.imageView);
                    imageView.resetZoom();
                }
            }

            @Override
            public void onPageSelected(int position) {
                selPicture = pictures.get(position);
                setTitle(selPicture.getName());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        LinearLayout bottomMenu = findViewById(R.id.bottomMenu);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomMenu);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        btnLocation = findViewById(R.id.btnLocation);
        intent = getIntent();
        selImageName = intent.getStringExtra("imageName");
        loadPictures();
    }

    private void loadPictures() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File mediaStorageDir = Utils.getPicturesDirectory();
                List<Picture> pictures1 = new ArrayList<>();
                List<File> files = Arrays.asList(mediaStorageDir.listFiles(IMAGE_FILTER));
                for (File file : files) {
                    try {
                        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                        double[] latLong = exif.getLatLong();
                        Picture picture = new Picture(file.getAbsolutePath(), file.getName());
                        if (latLong != null)
                            picture.setLatLong(latLong[0], latLong[1]);
                        pictures1.add(picture);
                        pictureNames.add(picture.getName());
                    } catch (IOException ignored) {
                    }
                }
                pictures.addAll(pictures1);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        viewAdapter.notifyDataSetChanged();
                        if (selImageName != null) {
                            if (pictureNames.contains(selImageName)) {
                                int i = pictureNames.indexOf(selImageName);
                                Picture picture = pictures.get(i);
                                showViewer();
                                viewPager.setCurrentItem(i, false);
                                setTitle(picture.getName());
                            }
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selPicture = pictures.get(i);
        showViewer();
        viewPager.setCurrentItem(i, false);
        if (!selPicture.hasLatLong)
            btnLocation.setEnabled(false);
        else
            btnLocation.setEnabled(true);
        setTitle(selPicture.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gallery_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isViewPagerVisible) {
                    hideViewer();
                    selImageName = null;
                    intent.removeExtra("imageName");
                    selPicture = null;
                    setTitle(R.string.gallery);
                } else {
                    finish();
                }
                return true;
            case R.id.camera:
                finish();
                startActivity(new Intent(this, CameraActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridView.setNumColumns(5);
        } else {
            gridView.setNumColumns(3);
        }
        super.onConfigurationChanged(newConfig);
    }

    private void showViewer() {
        viewPager.setVisibility(View.VISIBLE);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        isViewPagerVisible = true;
    }

    private void hideViewer() {
        viewPager.setVisibility(View.GONE);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        isViewPagerVisible = false;
    }

    public void onInfoClick(View v) {
        if (selPicture != null) {
            try {
                File file = new File(selPicture.getPath());
                if (file.exists()) {
                    ExifInterface exif = new ExifInterface(selPicture.getPath());
                    List<String> info = new ArrayList<>();
                    String nameInfo = "Name: " + selPicture.getName();
                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy:MM:dd HH:mm:ss");
                    DateTime dt = formatter.parseDateTime(exif.getAttribute(ExifInterface.TAG_DATETIME));
                    String timeInfo = "Time: " + dt.toString("MMM d, yyyy h:mm:ss a");
                    String widthInfo = "Width: " + exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH);
                    String heightInfo = "Height: " + exif.getAttribute(ExifInterface.TAG_IMAGE_LENGTH);
                    String byteUnit = " KB";
                    double fileSize = file.length() / 1024;
                    if (fileSize >= 1024) {
                        fileSize = fileSize / 1024;
                        byteUnit = " MB";
                    }
                    DecimalFormat dec = new DecimalFormat("0.00");
                    String fileSizeInfo = "File size: " + dec.format(fileSize).concat(byteUnit);
                    info.add(nameInfo);
                    info.add(timeInfo);
                    info.add(widthInfo);
                    info.add(heightInfo);
                    info.add(fileSizeInfo);
                    double[] latLong = exif.getLatLong();
                    if (selPicture.hasLatLong) {
                        String locationInfo = String.format(Locale.getDefault(), "Location:\n(%f, %f)", latLong[0], latLong[1]);
                        info.add(locationInfo);
                    }
                    InfoFragment infoFragment = new InfoFragment();
                    infoFragment.show(getSupportFragmentManager(), InfoFragment.TAG);
                    infoFragment.setItems(info);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onLocationClick(View v) {
        if (selPicture != null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Bundle bundle = new Bundle();
            bundle.putString("imageName", selPicture.getName());
            bundle.putString("imagePath", selPicture.getPath());
            bundle.putDoubleArray("imageLatLong", selPicture.getLatLong());
            intent.putExtra("imageInfo", bundle);
            finish();
            startActivity(intent);
        }
    }

    public void onRenameClick(View v) {
        if (selPicture != null) {
            String fileName = Utils.removeFileExtension(selPicture.getName());
            Utils.showNameDialog(this, "Rename picture", fileName, new Utils.NameDialogCallback() {
                @Override
                public void onPositiveClick(AlertDialog dialog, String name) {
                    final int nameIndex = pictureNames.indexOf(selPicture.getName());
                    final File file = new File(selPicture.getPath());
                    final File renameFile = Utils.newPictureFile(name);
                    if (!renameFile.exists()) {
                        if (file.renameTo(renameFile)) {
                            selPicture.setName(renameFile.getName());
                            selPicture.setPath(renameFile.getAbsolutePath());
                            pictureNames.set(nameIndex, renameFile.getName());
                            adapter.notifyDataSetChanged();
                            viewAdapter.notifyDataSetChanged();
                            GalleryActivity.this.setTitle(selPicture.getName());
                        } else {
                            Utils.toast(activity, "Failed to rename picture");
                        }
                    } else {
                        Utils.showDialog(activity, "Overwrite picture",
                                "The picture name already exists. Do you want to overwrite the picture?",
                                new Utils.DialogCallback() {
                                    @Override
                                    public void onPositiveClick(AlertDialog dialog) {
                                        int pictureIndex = pictureNames.indexOf(renameFile.getName());
                                        pictureNames.remove(pictureIndex);
                                        pictures.remove(pictureIndex);
                                        //noinspection ResultOfMethodCallIgnored
                                        renameFile.delete();
                                        if (file.renameTo(renameFile)) {
                                            selPicture.setName(renameFile.getName());
                                            selPicture.setPath(renameFile.getAbsolutePath());
                                            pictureNames.set(nameIndex, renameFile.getName());
                                            adapter.notifyDataSetChanged();
                                            viewAdapter.notifyDataSetChanged();
                                            GalleryActivity.this.setTitle(selPicture.getName());
                                        } else {
                                            Utils.toast(activity, "Failed to rename picture");
                                        }
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onNegativeClick(AlertDialog dialog) {
                                    }
                                });
                    }
                    dialog.dismiss();
                }

                @Override
                public void onNegativeClick(AlertDialog dialog) {
                }
            });
        }
    }

    public void onDeleteClick(View v) {
        if (selPicture != null) {
            Utils.showDialog(this, "Delete a picture", "Do you want to delete this picture?", new Utils.DialogCallback() {
                @Override
                public void onPositiveClick(AlertDialog dialog) {
                    File file = new File(selPicture.getPath());
                    if (file.delete()) {
                        pictureNames.remove(selPicture.getName());
                        pictures.remove(selPicture);
                        adapter.notifyDataSetChanged();
                        viewAdapter.notifyDataSetChanged();
                        hideViewer();
                    } else {
                        Utils.toast(activity, "Failed to delete picture");
                    }
                    dialog.dismiss();
                }

                @Override
                public void onNegativeClick(AlertDialog dialog) {
                }
            });
        }
    }

    //For gallery
    private static class PictureAdapter extends BaseAdapter {

        private Context context;
        private List<Picture> list;
        private LayoutInflater inflater;

        PictureAdapter(Context context, List<Picture> list) {
            this.context = context;
            this.list = list;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Picture getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder vh;
            if (view == null) {
                view = inflater.inflate(R.layout.picture_list_item, viewGroup, false);
                vh = new ViewHolder(view);
                view.setTag(vh);
            } else {
                vh = (ViewHolder) view.getTag();
            }
            Picture picture = getItem(i);
            File file = new File(picture.getPath());
            if (file.exists()) {
                GlideApp.with(context)
                        .load(file)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(vh.imageView);
            }
            return view;
        }

        private static class ViewHolder {
            ImageView imageView;

            ViewHolder(View v) {
                imageView = v.findViewById(R.id.imageView);
            }
        }
    }


    //For picture viewer
    private static class PictureViewAdapter extends PagerAdapter {

        private Context context;
        private List<Picture> list;
        private LayoutInflater inflater;

        PictureViewAdapter(Context context, List<Picture> list) {
            this.context = context;
            this.list = list;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.picture_view_item, container, false);
            final ImageView imageView = view.findViewById(R.id.imageView);
            Picture picture = list.get(position);
            File file = new File(picture.getPath());
            if (file.exists()) {
                try {
                    ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                    int width = exif.getAttributeInt(ExifInterface.TAG_IMAGE_WIDTH, 0);
                    int height = exif.getAttributeInt(ExifInterface.TAG_IMAGE_LENGTH, 0);
                    GlideApp.with(context)
                            .load(file)
                            .format(DecodeFormat.PREFER_ARGB_8888)
                            .override(width / 2, height / 2)
                            .into(new SimpleTarget<Drawable>() {
                                @Override
                                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                                    imageView.setImageDrawable(resource);
                                }
                            });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            view.setTag("view" + position);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }

}
