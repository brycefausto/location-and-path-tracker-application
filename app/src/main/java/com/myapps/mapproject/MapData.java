package com.myapps.mapproject;

import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;

import org.json.JSONObject;

public class MapData {

    String mapName, mapDate;
    long mapSize;
    OfflineTilePyramidRegionDefinition definition;

    public MapData(String mapName, String mapDate, long mapSize, OfflineTilePyramidRegionDefinition definition) {
        this.mapName = mapName;
        this.mapDate = mapDate;
        this.mapSize = mapSize;
        this.definition = definition;
    }

    public static MapData offlineRegionToMapData(OfflineRegion offlineRegion) {
        byte[] metadata = offlineRegion.getMetadata();
        try {
            String json = new String(metadata, IMap.JSON_CHARSET);
            JSONObject jsonObject = new JSONObject(json);
            String mapName = jsonObject.getString(IMap.JSON_FIELD_REGION_NAME);
            String mapDate = jsonObject.getString(IMap.JSON_FIELD_MAP_DATE);
            long mapSize = jsonObject.getLong(IMap.JSON_FIELD_MAP_SIZE);
            return new MapData(mapName, mapDate, mapSize, (OfflineTilePyramidRegionDefinition) offlineRegion.getDefinition());
        } catch (Exception exception) {
            return null;
        }
    }
}
