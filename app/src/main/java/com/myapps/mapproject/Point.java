package com.myapps.mapproject;

public class Point {

    double latitude, longitude;

    public Point(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
