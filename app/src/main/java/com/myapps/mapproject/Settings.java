package com.myapps.mapproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Settings {

    private static SharedPreferences pref;
    private static String keyTimeInterval, keyTrackFacing, keyShowBlueDot;
    private static int timeInterval = 10;
    private static boolean trackFacing = true, showBlueDot = false;


    public static void load(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        keyTimeInterval = context.getString(R.string.key_time_interval);
        keyTrackFacing = context.getString(R.string.key_track_facing);
        keyShowBlueDot = context.getString(R.string.key_show_blue_dot);
    }

    public static int getGPSTimeInterval() {
        if (pref == null) return timeInterval;
        return pref.getInt(keyTimeInterval, timeInterval);
    }

    public static boolean isTrackFacing() {
        if (pref == null) return trackFacing;
        return pref.getBoolean(keyTrackFacing, trackFacing);
    }

    public static boolean isShowBlueDot() {
        if (pref == null) return showBlueDot;
        return pref.getBoolean(keyShowBlueDot, showBlueDot);
    }

}
