package com.myapps.mapproject;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public abstract class HttpCallback implements Callback {

    @Override
    public final void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onFailure(e);
            }
        });
    }

    @Override
    public final void onResponse(Call call, final Response response) throws IOException {
        final String responseStr = response.body().string();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    onResponse(response, responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public abstract void onFailure(IOException e);

    public abstract void onResponse(Response response, String responseStr) throws IOException;

}
