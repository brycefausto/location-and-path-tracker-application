package com.myapps.mapproject;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.ClusterItem;

public class MyItem implements ClusterItem {

    private LatLng position;
    private String title, snippet;
    private String imageName, imagePath;

    public MyItem(LatLng position, String title) {
        this.position = position;
        this.title = title;
    }

    public MyItem(LatLng position, String imageName, String imagePath) {
        this.position = position;
        this.imageName = imageName;
        this.imagePath = imagePath;
        snippet = "View image";
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImagePath() {
        return imagePath;
    }
}
