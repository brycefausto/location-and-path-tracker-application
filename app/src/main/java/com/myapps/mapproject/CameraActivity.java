package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.media.ExifInterface;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.OnSuccessListener;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class CameraActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private final Activity activity = this;

    private TextView textView;
    private ImageView imageView;
    private FusedLocationProviderClient locationProviderClient;
    private Bitmap image;
    private String tempFilePath, savedImageName;

    public static String convertLatOrLng(double latOrLng) {
        StringBuilder sb = new StringBuilder();
        latOrLng = Math.abs(latOrLng);
        final int degree = (int) latOrLng;
        latOrLng *= 60;
        latOrLng -= degree * 60.0d;
        final int minute = (int) latOrLng;
        latOrLng *= 60;
        latOrLng -= minute * 60.0d;
        final int second = (int) (latOrLng * 1000.0d);
        sb.setLength(0);
        sb.append(degree);
        sb.append("/1,");
        sb.append(minute);
        sb.append("/1,");
        sb.append(second);
        sb.append("/1000,");
        return sb.toString();
    }

    public static String latitudeRef(final double latitude) {
        return latitude < 0.0d ? "S" : "N";
    }

    public static String longitudeRef(final double longitude) {
        return longitude < 0.0d ? "W" : "E";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textView = findViewById(R.id.textView);
        imageView = findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (savedImageName != null) {
                    Intent intent = new Intent(activity, GalleryActivity.class);
                    intent.putExtra("imageName", savedImageName);
                    startActivity(intent);
                    finish();
                }
            }
        });
        Utils.setLocationSettings(this);
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        takePicture();
    }

    public void onOpenCameraClick(View v) {
        takePicture();
    }

    private void takePicture() {
        File dir = getTempDir();
        File file;
        try {
            file = File.createTempFile(createTempImageName(), ".tmp", dir);
            tempFilePath = file.getAbsolutePath();
        } catch (IOException e) {
            Utils.toast(activity, "Error occurred while creating a temp file");
            return;
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Uri uri = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", file);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File getTempDir() {
        File dir = new File(getExternalFilesDir(null), "temp");
        if (!dir.exists() && !dir.mkdir())
            return null;
        return dir;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                textView.setVisibility(View.GONE);
                image = BitmapFactory.decodeFile(tempFilePath);
                imageView.setImageBitmap(image);
                deleteDCIMFile();
                Utils.setLocationSettings(activity, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        TrackingService.getLastKnownLocation(locationProviderClient, new Utils.LocationResultCallback() {
                            @Override
                            void onLocationResult(Location location) {
                                showSaveDialog(location);
                            }
                        });
                    }
                });
            } else {
                finish();
            }
            tempFilePath = null;
            File dir = getTempDir();
            for (File file : dir.listFiles()) {
                file.delete();
            }
        }
        if (requestCode == Utils.REQUEST_CHECK_SETTINGS && resultCode == RESULT_CANCELED)
            finish();
    }

    private void deleteDCIMFile() {
        File f = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera");
        File[] files = f.listFiles();
        Arrays.sort(files, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                    return -1;
                } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                    return 1;
                } else {
                    return 0;
                }
            }

        });
        files[0].delete();
    }

    public void onGalleryClick(View v) {
        startActivity(new Intent(this, GalleryActivity.class));
    }

    private String createTempImageName() {
        DateTime dt = new DateTime();
        return "IMG_" + dt.toString("yyyyMMdd_HHmmss");
    }

    private void showSaveDialog(final Location location) {
        String imageName = createTempImageName();
        PictureDialogFragment fragment = PictureDialogFragment.newInstance(imageName);
        fragment.show(getSupportFragmentManager(), fragment.getTag());
        fragment.setListener(new PictureDialogFragment.DialogListener() {
            @Override
            public void onDone(String imageName) {
                savePicture(imageName, location);
            }

            @Override
            public void onCancel() {
                image = null;
                savedImageName = null;
                textView.setVisibility(View.VISIBLE);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.borders));
            }
        });
    }

    private void savePicture(final String imageName, final Location location) {
        final Activity activity = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Utils.getPicturesDirectory() == null) {
                    Utils.toastOnUI(activity, "Failed to create directory");
                    return;
                }
                File pictureFile = Utils.newPictureFile(imageName);
                if (pictureFile == null) {
                    Utils.toastOnUI(activity, "Failed to create file");
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.close();
                } catch (IOException e) {
                    Utils.toastOnUI(activity, "Error saving file");
                    return;
                }
                image = null;

                Double latitude = location.getLatitude();
                Double longitude = location.getLongitude();
                try {
                    ExifInterface exif = new ExifInterface(pictureFile.getAbsolutePath());
                    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, convertLatOrLng(latitude));
                    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, convertLatOrLng(longitude));
                    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, latitudeRef(latitude));
                    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, longitudeRef(longitude));
                    DateTime dt = new DateTime(location.getTime());
                    exif.setAttribute(ExifInterface.TAG_DATETIME, dt.toString("yyyy:MM:dd HH:mm:ss"));
                    exif.saveAttributes();
                } catch (IOException e) {
                    Utils.toastOnUI(activity, "Error tagging file");
                }
                savedImageName = pictureFile.getName();
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaScanIntent.setData(Uri.fromFile(pictureFile));
                sendBroadcast(mediaScanIntent);
            }
        }).start();
    }

}
