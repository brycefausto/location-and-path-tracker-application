package com.myapps.mapproject;


public class IMap {
    public static final String JSON_CHARSET = "UTF-8";
    public static final String JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";
    public static final String JSON_FIELD_MAP_DATE = "FIELD_MAP_DATE";
    public static final String JSON_FIELD_MAP_SIZE = "FIELD_MAP_SIZE";
}
