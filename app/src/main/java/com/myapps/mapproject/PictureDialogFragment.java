package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.EditText;

public class PictureDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private DialogListener listener;
    private EditText editImageName;

    public static PictureDialogFragment newInstance(String imageName) {
        Bundle bundle = new Bundle();
        bundle.putString("imageName", imageName);
        PictureDialogFragment fragment = new PictureDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.fragment_picture_dialog, null);
        dialog.setContentView(view);
        editImageName = view.findViewById(R.id.editImageName);
        view.findViewById(R.id.btnDone).setOnClickListener(this);
        view.findViewById(R.id.btnCancel).setOnClickListener(this);
        String imageName = getArguments().getString("imageName");
        editImageName.setText(imageName);
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            switch (view.getId()) {
                case R.id.btnDone:
                    String imageName = editImageName.getText().toString();
                    if (!imageName.equals("")) {
                        listener.onDone(imageName);
                        dismiss();
                    } else {
                        Utils.toast(getContext(), "Enter an image name");
                    }
                    break;
                case R.id.btnCancel:
                    listener.onCancel();
                    dismiss();
                    break;
            }
        }
    }

    public void setListener(DialogListener listener) {
        this.listener = listener;
    }

    public interface DialogListener {
        void onDone(String imageName);

        void onCancel();
    }
}
