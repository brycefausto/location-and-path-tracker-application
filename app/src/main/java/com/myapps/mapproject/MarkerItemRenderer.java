package com.myapps.mapproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;

public class MarkerItemRenderer extends DefaultClusterRenderer<MarkerItem> {

    private Context context;
    private IconGenerator clusterIconGenerator, itemIconGenerator;
    private TextView txtImageName;
    private ImageView imageView, imgMarkerImage;
    private Drawable placeholder;

    public MarkerItemRenderer(Context context, GoogleMap map, ClusterManager<MarkerItem> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
        clusterIconGenerator = new IconGenerator(context);
        itemIconGenerator = new IconGenerator(context);
        View view = LayoutInflater.from(context).inflate(R.layout.image_cluster, null);
        imageView = view.findViewById(R.id.imageView);
        clusterIconGenerator.setContentView(view);
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_marker, null);
        txtImageName = view1.findViewById(R.id.txtImageName);
        imgMarkerImage = view1.findViewById(R.id.imgMarkerImage);
        itemIconGenerator.setContentView(view1);
        placeholder = context.getResources().getDrawable(R.drawable.borders);
    }

    @Override
    protected void onBeforeClusterItemRendered(MarkerItem item, MarkerOptions markerOptions) {
        if (item.getImagePath() != null) {
            txtImageName.setText("");
            imgMarkerImage.setImageDrawable(placeholder);
            Bitmap icon = itemIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MarkerItem> cluster, MarkerOptions markerOptions) {
        imageView.setImageDrawable(placeholder);
        Bitmap icon = clusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("View images");
    }

    @Override
    protected void onClusterItemRendered(final MarkerItem clusterItem, final Marker marker) {
        GlideApp.with(context)
                .load(clusterItem.getImagePath())
                .override(120)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        if (getClusterItem(marker) != null) {
                            txtImageName.setText(clusterItem.getImageName());
                            imgMarkerImage.setImageDrawable(resource);
                            Bitmap icon = itemIconGenerator.makeIcon();
                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
                        }
                    }
                });
    }

    @Override
    protected void onClusterRendered(final Cluster<MarkerItem> cluster, final Marker marker) {
        final List<Drawable> imageDrawables = new ArrayList<>(Math.min(4, cluster.getSize()));
        final int width = imageView.getWidth();
        final int height = imageView.getHeight();
        final int[] counter = {0};
        for (MarkerItem item : cluster.getItems()) {
            if (counter[0] == 4) break;
            counter[0]++;
            GlideApp.with(context)
                    .load(item.getImagePath())
                    .override(100)
                    .centerCrop()
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            resource.setBounds(0, 0, width, height);
                            imageDrawables.add(resource);
                            if (imageDrawables.size() == counter[0] && getCluster(marker) != null) {
                                MultiDrawable multiDrawable = new MultiDrawable(imageDrawables);
                                multiDrawable.setBounds(0, 0, width, height);
                                imageView.setImageDrawable(multiDrawable);
                                Bitmap icon = clusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                                marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
                            }
                        }
                    });
        }
    }

    protected void createImageMarker(final Marker marker, final String imageName, String imagePath) {
        GlideApp.with(context)
                .load(imagePath)
                .override(120)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        txtImageName.setText(imageName);
                        imgMarkerImage.setImageDrawable(resource);
                        Bitmap icon = itemIconGenerator.makeIcon();
                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
                    }
                });
        Bitmap icon = itemIconGenerator.makeIcon();
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
    }

}
