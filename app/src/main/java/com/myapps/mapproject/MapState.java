package com.myapps.mapproject;

import android.content.SharedPreferences;

import com.google.gson.Gson;

//Contains saved data of the map
public class MapState {

    public static final int TYPE_NORMAL = 0, TYPE_SATELLITE = 1, TYPE_HYBRID = 2;
    private static final String KEY_MAP_STATE = "mapState";
    private static MapState instance;
    float cameraZoom, cameraZoom1, cameraBearing, cameraTilt;
    int mapType;
    long selPathId;
    boolean isMarkerInfoShown, isPathsShown, isImagesShown, isMapBox;
    String markerTitle;
    String markerImagePath;
    Point cameraTarget, markerPosition, locationPosition;

    private MapState() {
        double defLatitude = 14.5959895;
        double defLongitude = 120.9689141;
        cameraTarget = new Point(defLatitude, defLongitude);
        cameraZoom = 14;
        cameraBearing = 0;
        cameraTilt = 0;
    }

    static MapState getInstance() {
        if (instance == null)
            instance = new MapState();
        return instance;
    }

    static MapState load(SharedPreferences pref) {
        MapState mapState = null;
        if (instance == null) {
            String jsonStr = pref.getString(KEY_MAP_STATE, "");
            if (!jsonStr.equals("")) {
                mapState = new Gson().fromJson(jsonStr, MapState.class);
                instance = mapState;
            }
        } else {
            return instance;
        }
        if (mapState == null) {
            mapState = getInstance();
        }
        return mapState;
    }

    static void removeInstance() {
        instance = null;
    }

    void save(SharedPreferences pref) {
        String jsonStr = new Gson().toJson(this);
        pref.edit().putString(KEY_MAP_STATE, jsonStr).apply();
    }

    public void setCameraZoom(float zoom) {
        cameraZoom = zoom;
        cameraZoom1 = zoom - 1;
    }

    public void setCameraZoom1(double zoom) {
        cameraZoom1 = (float) zoom;
        cameraZoom = (float) zoom + 1;
    }

}
