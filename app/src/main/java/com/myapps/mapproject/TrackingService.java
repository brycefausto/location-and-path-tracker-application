package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.opengl.Matrix;
import android.os.IBinder;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

//Used to track the path of the user
public class TrackingService extends Service implements SensorEventListener {

    private static final double NS2S = 0.000000001;
    private static final float SHAKE_THRESHOLD = 2f;

    public static boolean isServiceRunning;
    public static int screenOrientation;
    public static Location lastKnownLocation;
    private static List<Point> pathPoints;
    private KalmanFilter kFX = new KalmanFilter();
    private KalmanFilter kFY = new KalmanFilter();
    private FusedLocationProviderClient providerClient;
    private Location lastLocation, lastSensorLocation;
    private SensorManager sensorManager;
    private List<Sensor> sensors = new ArrayList<>();
    private Sensor accSensor, linearAccSensor, gravitySensor, magneticSensor, gyroSensor;
    private float[] linearAcc, lastLinearAcc, gravity, mag;
    private float[] rMatrix, deltaRV;
    private float compassBearing = 0;
    private double[] v = {0, 0}, d = {0, 0};
    private double timestampAcc, timeStampGyro, totalDeltaTime;
    private boolean hasFirstLocation, isRequestingLocationUpdates, isStable = true;

    private List<Float> accList = new ArrayList<>();

    public TrackingService() {
    }

    public static List<Point> getPathPoints() {
        if (pathPoints == null) pathPoints = new ArrayList<>();
        return pathPoints;
    }

    public static void getLastKnownLocation(FusedLocationProviderClient locationProviderClient, final Utils.LocationResultCallback callback) {
        Utils.requestLocationUpdates(locationProviderClient, 3000, true, new Utils.LocationResultCallback() {
            @Override
            void onLocationResult(Location location) {
                if (lastKnownLocation != null && lastKnownLocation.distanceTo(location) <= 40
                        && lastKnownLocation.getAccuracy() < location.getAccuracy()
                        && (lastKnownLocation.getTime() - location.getTime()) < 3600000)
                    location = lastKnownLocation;
                callback.onLocationResult(location);
                lastKnownLocation = location;
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isServiceRunning = true;
        providerClient = LocationServices.getFusedLocationProviderClient(this);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        linearAccSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        gyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        int sensorDelay = SensorManager.SENSOR_DELAY_UI;
        sensorManager.registerListener(this, accSensor, sensorDelay);
        sensors.add(accSensor);
        if (linearAccSensor != null && gravitySensor != null) {
            sensorManager.registerListener(this, linearAccSensor, sensorDelay);
            sensorManager.registerListener(this, gravitySensor, sensorDelay);
            sensors.add(linearAccSensor);
            sensors.add(gravitySensor);
        }
        if (magneticSensor != null) {
            sensorManager.registerListener(this, magneticSensor, sensorDelay);
            sensors.add(magneticSensor);
        }
        if (gyroSensor != null) {
            sensorManager.registerListener(this, gyroSensor, sensorDelay);
            sensors.add(gyroSensor);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setForeground();
        requestLocationUpdates();
        return START_STICKY;
    }

    private void requestLocationUpdates() {
        isRequestingLocationUpdates = true;
        Utils.requestLocationUpdates(providerClient, Settings.getGPSTimeInterval() * 1000, false, locationResultCallback);
    }

    private Utils.LocationResultCallback locationResultCallback = new Utils.LocationResultCallback() {
        @Override
        void onLocationResult(Location location) {
            if (lastLocation != null) {
                location.setBearing(lastLocation.bearingTo(location));
                location.setSpeed(Utils.getSpeed(location, lastLocation));
                float bearing = lastLocation.getBearing();
                float bearing1 = location.getBearing();
                if (!lastLocation.hasBearing()) bearing = bearing1;
                float velocity = lastLocation.getSpeed();
                float velocity1 = location.getSpeed();
                if (!lastLocation.hasSpeed()) velocity = velocity1;
                float[] velocityXY = Utils.getXYFromLocation(velocity, bearing);
                if (magneticSensor != null) bearing1 = compassBearing;
                float[] velocityXY1 = Utils.getXYFromLocation(velocity1, bearing1);
                float dt = Utils.getDeltaTime(location, lastLocation);
                float accX = (velocityXY1[0] - velocityXY[0]) / dt;
                float accY = (velocityXY1[1] - velocityXY[1]) / dt;
                float dist = lastLocation.distanceTo(location);
                float[] distXY = Utils.getXYFromLocation(dist, bearing1);
                float accuracy = lastLocation.getAccuracy();
                float accuracy1 = location.getAccuracy();
                float speedAccuracy = Utils.getSpeedAccuracy(lastLocation);
                float speedAccuracy1 = Utils.getSpeedAccuracy(location);
                Utils.log("dist: " + dist);
                Utils.log("distX: " + distXY[0]);
                Utils.log("distY: " + distXY[1]);
                Utils.log("deltaTime: " + dt);
                Utils.log("speed: " + velocity1);
                Utils.log("speedX: " + velocityXY1[0]);
                Utils.log("speedY: " + velocityXY1[1]);
                Utils.log("bearing: " + bearing1);
                Utils.log("accuracy: " + location.getAccuracy());
                Utils.log("speed accuracy: " + speedAccuracy1);
                double posError = Math.max(accuracy - kFX.getAccuracy(), 0);
                double velError = Math.max(speedAccuracy - kFX.getSpeedAccuracy(), 0);
                if (magneticSensor != null) {
                    posError *= 0.25;
                    velError *= 0.25;
                }
                Utils.log("posError: " + posError);
                Utils.log("velError: " + velError);
                double[] estX = kFX.estimate(0, dt, distXY[0], velocityXY1[0], posError, velError, accuracy1, speedAccuracy1);
                double[] estY = kFY.estimate(0, dt, distXY[1], velocityXY1[1], posError, velError, accuracy1, speedAccuracy1);
                double[] latLong = Utils.computeOffset(lastLocation.getLatitude(), lastLocation.getLongitude(), estY[0], estX[0]);
                float accuracy2 = kFX.getAccuracy();
                float bearing2 = (float) Math.atan2(estX[0], estY[0]);
                if (magneticSensor != null) bearing2 = compassBearing;
                Location locationNew = new Location("Estimate");
                locationNew.setLatitude(latLong[0]);
                locationNew.setLongitude(latLong[1]);
                locationNew.setAccuracy(accuracy2);
                locationNew.setSpeed((float) Utils.hypot(estX[1], estY[1]));
                locationNew.setBearing(bearing2);
                Utils.log("est dist: " + Utils.hypot(estX[0], estY[0]));
                Utils.log("est distX: " + estX[0]);
                Utils.log("est distY: " + estY[0]);
                Utils.log("est speed: " + Utils.hypot(estX[1], estY[1]));
                Utils.log("est speedX: " + estX[1]);
                Utils.log("est speedY: " + estY[1]);
                Utils.log("est bearing: " + Math.atan2(estX[0], estY[0]));
                location = locationNew;
                Utils.toast(TrackingService.this, "New Location");
            } else {
                float accuracy = location.getAccuracy();
                if (magneticSensor != null) {
                    accuracy *= 0.25;
                }
                kFX.setError(accuracy, accuracy);
                kFY.setError(accuracy, accuracy);
                Utils.log("KF acc: " + location.getAccuracy());
            }
            postLocation(location);
            lastLocation = location;
            if (!isStable) {
                isRequestingLocationUpdates = false;
                Utils.removeLocationUpdates();
            }
            //TODO
                /*
                 if (!hasFirstLocation) {
                    hasFirstLocation = true;
                    postLocation(location);
                }
                if (hasFirstLocation && lastSensorLocation != null && location.getAccuracy() < 10 && location.distanceTo(lastSensorLocation) < 6) {
                    lastSensorLocation = null;
                }
               */
        }
    };

    private void postLocation(Location location) {
        Point point = new Point(location.getLatitude(), location.getLongitude());
        getPathPoints().add(point);
        if (!MapApp.isAppBackground()) {
            EventBus.getDefault().post(location);
        }
    }

    private void setForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle("MapData Project is tracking")
                .setContentText("The app is currently tracking your path")
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceRunning = false;
        pathPoints = null;
        lastLocation = null;
        Utils.removeLocationUpdates();
        for (Sensor sensor : sensors) {
            sensorManager.unregisterListener(this, sensor);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] values = sensorEvent.values;
        long timestamp = sensorEvent.timestamp;
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                onChangeAccelerometer(values, timestamp);
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                linearAcc = values.clone();
                break;
            case Sensor.TYPE_GRAVITY:
                gravity = values.clone();
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mag = values.clone();
                break;
            case Sensor.TYPE_GYROSCOPE:
                onChangeGyroscope(values, timestamp);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onChangeAccelerometer(float[] values, long timestamp) {
        if (linearAccSensor == null && gravitySensor == null) {
            linearAcc = new float[3];
            gravity = new float[3];
            final float alpha = 0.1f;
            if (gravity == null) gravity = new float[3];
            for (int i = 0; i < gravity.length; i++) {
                gravity[i] = alpha * gravity[i] + (1 - alpha) * values[i];
                linearAcc[i] = values[i] - gravity[i];
            }
        }
        double deltaTime = 0;
        if (timestampAcc != 0) {
            deltaTime = (timestamp - timestampAcc) * NS2S;
            totalDeltaTime += deltaTime;
        }
        timestampAcc = timestamp;
        //TODO
        if (linearAcc != null) {
            float lw = Utils.hypot3(linearAcc[0], linearAcc[1], linearAcc[2]);
            accList.add(lw);
            float stability = Math.abs(lw);
            isStable = stability <= 0.2;
        }
        if (!isStable && !isRequestingLocationUpdates) {
            requestLocationUpdates();
        }
        if (mag != null && gravity != null && isStable) {
            rMatrix = new float[16];
            SensorManager.getRotationMatrix(rMatrix, null, gravity, mag);
            if (deltaRV != null) {
                Utils.calculateFusedMatrix(rMatrix, deltaRV);
                deltaRV = null;
            }
        }
        boolean isShaking = false;
        if (lastLinearAcc != null) {
            float[] dAcc = new float[3];
            for (int i = 0; i < dAcc.length; i++) {
                dAcc[i] = Math.abs(lastLinearAcc[i] - linearAcc[i]);
            }
            /*
            if ((dAcc[0] > SHAKE_THRESHOLD && dAcc[1] > SHAKE_THRESHOLD) ||
                    (dAcc[0] > SHAKE_THRESHOLD && dAcc[2] > SHAKE_THRESHOLD) ||
                    (dAcc[1] > SHAKE_THRESHOLD && dAcc[2] > SHAKE_THRESHOLD)) {
                isShaking = true;
                Log.d("msg1", "shaking");
            }
            */
        }
        if (linearAcc != null)
            lastLinearAcc = linearAcc.clone();
        if (rMatrix != null && linearAcc != null && !isStable && !isShaking) {
            float[] acc = {0, linearAcc[1], linearAcc[2], 0};
            float[] earthAcc = new float[4];
            Matrix.multiplyMV(earthAcc, 0, rMatrix, 0, acc, 0);
            for (int i = 0; i < v.length; i++) {
                d[i] = d[i] + (v[i] * deltaTime + earthAcc[i] * deltaTime * deltaTime * 0.5);
                v[i] = v[i] + earthAcc[i] * deltaTime;
                /*
                String axis = (i == 0) ? "x" : "y";
                Log.d("msg1", "d" + axis + ": " + (v[i] * deltaTime + earthAcc[i] * deltaTime * deltaTime * 0.5));
                */
            }
        }
        if (totalDeltaTime >= 1) {
            float[] angles = new float[3];
            if (rMatrix != null) {
                angles = SensorManager.getOrientation(rMatrix, angles);
                compassBearing = (float) Math.toDegrees(angles[0]);
            }
            d[0] *= 3.5;
            d[1] *= 3.5;
            double distance = Utils.hypot(d[0], d[1]);

            //Log.d("msg1", "distance: " + distance);
            /*
            //TODO
            float sumAcc = 0;
            for (float acc: accList) {
                sumAcc += acc;
            }
            float mean = sumAcc / accList.size();
            List<Float> accDiffList = new ArrayList<>();
            for (float acc: accList) {
                accDiffList.add(acc - mean);
            }
            float variance = 0;
            for (float acc: accDiffList) {
                variance += (acc * acc);
            }
            variance = variance / accDiffList.size() - 1;
            float sd = (float) Math.sqrt(variance);
            Log.d("msg1", "Standard Deviation: " + sd);
            */

            if (distance >= 1) {
                if (lastLocation != null) {
                    /*
                    Location location = lastSensorLocation;
                    if (lastSensorLocation == null) location = lastLocation;
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    bearing = Utils.getTrueBearing(bearing, latitude, longitude);
                    double latLong[] = Utils.computeOffset(latitude, longitude, d[1], d[0]);
                    float speed = (float) Utils.hypot(v[0], v[1]);
                    Location locationNew = new Location("Sensor");
                    locationNew.setLatitude(latLong[0]);
                    locationNew.setLongitude(latLong[1]);
                    locationNew.setAccuracy(location.getAccuracy());
                    locationNew.setTime(System.currentTimeMillis());
                    locationNew.setBearing(bearing);
                    locationNew.setSpeed(speed);
                    postLocation(locationNew);
                    lastSensorLocation = locationNew;
                    lastKnownLocation = locationNew;
                    */
                }
            }
            v = new double[]{0, 0};
            d = new double[]{0, 0};
            totalDeltaTime = 0;
        }
    }

    public void onChangeGyroscope(float[] values, long timestamp) {
        float deltaTime = 0;
        if (timeStampGyro != 0) {
            deltaTime = (float) ((timestamp - timeStampGyro) * NS2S);
        }
        timeStampGyro = timestamp;
        float epsilon = 0.000000001f;
        float omega = Utils.hypot3(values[0], values[1], values[2]);
        if (omega > epsilon) {
            for (int i = 0; i < values.length; i++) {
                values[i] /= omega;
            }
        }
        float tO2 = omega * deltaTime / 2.0f;
        float sinTO2 = (float) Math.sin(tO2);
        float cosTO2 = (float) Math.cos(tO2);
        deltaRV = new float[4];
        for (int i = 0; i < values.length; i++) {
            deltaRV[i] = sinTO2 * values[i];
        }
        deltaRV[3] = cosTO2;
    }

}
