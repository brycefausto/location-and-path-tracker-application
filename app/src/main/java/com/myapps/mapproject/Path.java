package com.myapps.mapproject;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Path {

    @Id(autoincrement = true)
    private Long id;

    @Property
    private long userId;

    @Property
    private String pathName;

    @Property
    private String dateTime;

    @Property
    private String points;

    @Transient
    private transient List<Point> pointsLatLong;

    public Path() {
    }

    @Generated(hash = 674408049)
    public Path(Long id, long userId, String pathName, String dateTime,
                String points) {
        this.id = id;
        this.userId = userId;
        this.pathName = pathName;
        this.dateTime = dateTime;
        this.points = points;
    }

    public Path(Long id, long userId, String pathName, String dateTime,
                List<Point> points) {
        this.id = id;
        this.userId = userId;
        this.pathName = pathName;
        this.dateTime = dateTime;
        this.pointsLatLong = points;
        setPoints(points);
    }

    private static String serializePath(List<Point> pathPoints) {
        StringBuilder stringBuilder = new StringBuilder();
        int lastIndex = pathPoints.size() - 1;
        for (int i = 0; i < pathPoints.size(); i++) {
            Point point = pathPoints.get(i);
            String pointStr = point.latitude + "," + point.longitude;
            stringBuilder.append(pointStr);
            if (i < lastIndex) {
                stringBuilder.append("-");
            }
        }
        return stringBuilder.toString();
    }

    private static List<Point> parsePath(String pathStr) {
        List<Point> pathPoints = new ArrayList<>();
        String[] pointsStr = pathStr.split("-");
        for (String pointStr : pointsStr) {
            String[] latLngStr = pointStr.split(",");
            float lat = Float.parseFloat(latLngStr[0]);
            float lng = Float.parseFloat(latLngStr[1]);
            pathPoints.add(new Point(lat, lng));
        }
        return pathPoints;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPoints() {
        return this.points;
    }

    public void setPoints(String points) {
        this.points = points;
        this.pointsLatLong = parsePath(points);
    }

    public void setPoints(List<Point> pathPoints) {
        pointsLatLong = pathPoints;
        this.points = serializePath(pathPoints);
    }

    public List<Point> getPointsLatLong() {
        if (pointsLatLong == null)
            pointsLatLong = parsePath(this.points);
        return pointsLatLong;
    }

    public String getPathName() {
        return this.pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

}
