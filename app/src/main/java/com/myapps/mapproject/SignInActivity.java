package com.myapps.mapproject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignInActivity extends AppCompatActivity implements EditText.OnKeyListener {

    private static final int RC_SIGN_IN = 3;

    private final Activity activity = this;

    private EditText editUsername, editPassword;
    private ProgressBar progressBar;
    private UserDao userDao;
    private SharedPreferences pref;
    private GoogleSignInClient googleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        progressBar = findViewById(R.id.progressBar);
        findViewById(R.id.btnGoogleSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleSignIn();
            }
        });
        pref = getSharedPreferences("pref", MODE_PRIVATE);
        DaoSession daoSession = ((MapApp) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
                keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER &&
                !editUsername.getText().toString().equals("")) {
            signIn();
            return true;
        }
        return false;
    }

    public void onButtonClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignIn:
                signIn();
                break;
            case R.id.btnRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    private void signIn() {
        //Username input can be username or email
        String username = editUsername.getText().toString().toLowerCase();
        String password = editPassword.getText().toString();
        if (!(Utils.isValidUsernameEmail(this, username) && Utils.isValidPassword(this, password))) {
            return;
        }
        onlineSignIn(User.newLoginInfo(username, password), false);
    }

    private void finishSignIn(long id) {
        User.saveSignIn(id, pref);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void googleSignIn() {
        Intent intent = googleSignInClient.getSignInIntent();
        startActivityForResult(intent, RC_SIGN_IN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        progressBar.setVisibility(View.VISIBLE);
        if (resultCode == RESULT_OK && requestCode == RC_SIGN_IN) {
            GoogleSignIn.getSignedInAccountFromIntent(data).addOnCompleteListener(new OnCompleteListener<GoogleSignInAccount>() {
                @Override
                public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                    progressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    try {
                        GoogleSignInAccount account = task.getResult(ApiException.class);
                        checkGoogleAccount(account.getEmail(), account.getDisplayName());
                        googleSignInClient.revokeAccess().addOnCompleteListener(null);
                    } catch (ApiException e) {
                        Utils.toast(activity, "Google sign in error");
                    }
                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void checkGoogleAccount(final String email, final String fullName) {
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("username", email)
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/check_account.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                //Check google account in local database
                progressBar.setVisibility(View.GONE);
                User user = userDao.queryBuilder().where(UserDao.Properties.Email.eq(email)).unique();
                if (user != null) {
                    enterPassword(user);
                } else {
                    Utils.toast(activity, "Sign in failed");
                }
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                progressBar.setVisibility(View.GONE);
                User user = new User(email, fullName);
                if (response.isSuccessful() && responseStr.equals("exists")) {
                    enterPassword(user);
                } else if (responseStr.equals("not exists")) {
                    setPassword(user);
                } else {
                    Utils.toast(activity, "Sign in failed");
                }
            }
        });
    }

    //For registering google account
    private void setPassword(final User user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = activity.getLayoutInflater().inflate(R.layout.set_password_dialog, null);
        TextView txtEmail = v.findViewById(R.id.txtEmail);
        txtEmail.setText(user.getEmail());
        final EditText editPassword = v.findViewById(R.id.editPassword);
        builder.setView(v);
        Utils.showDialog(builder, "Register password", "Register your password for this app", new Utils.DialogCallback() {
            @Override
            public void onPositiveClick(AlertDialog dialog) {
                String password = editPassword.getText().toString();
                if (Utils.isValidPassword(activity, password)) {
                    user.setPassword(password);
                    registerGoogleAccount(user);
                }
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(AlertDialog dialog) {
            }
        });
    }

    //For signing in google account
    private void enterPassword(final User user) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = activity.getLayoutInflater().inflate(R.layout.set_password_dialog, null);
        TextView txtEmail = v.findViewById(R.id.txtEmail);
        txtEmail.setText(user.getEmail());
        final EditText editPassword = v.findViewById(R.id.editPassword);
        builder.setView(v);
        Utils.showDialog(builder, "Enter your password", "Enter your login password for this app", new Utils.DialogCallback() {
            @Override
            public void onPositiveClick(AlertDialog dialog) {
                String password = editPassword.getText().toString();
                if (Utils.isValidPassword(activity, password)) {
                    user.setPassword(password);
                    onlineSignIn(user, true);
                }
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(AlertDialog dialog) {
            }
        });
    }

    private void onlineSignIn(final User user, final boolean isGoogleSignIn) {
        progressBar.setVisibility(View.VISIBLE);
        final String username = user.getUsername();
        final String password = user.getPassword();
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("username", user.getUsername())
                .add("password", user.getPassword())
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/sign_in.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                progressBar.setVisibility(View.GONE);
                //Check user in local database
                User user1 = userDao.queryBuilder()
                        .whereOr(UserDao.Properties.Username.eq(username), UserDao.Properties.Email.eq(username)).unique();
                if (user1 != null && password.equals(user.getPassword())) {
                    finishSignIn(user1.getId());
                } else if (user1 != null && !password.equals(user1.getPassword())) {
                    Utils.toast(activity, "The password is incorrect");
                    editPassword.setText("");
                } else {
                    Utils.toast(activity, "This account does not exists");
                    editUsername.setText("");
                    editPassword.setText("");
                }
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(responseStr);
                        long id = Long.parseLong(jsonObject.getString("id"));
                        String username = jsonObject.getString("username");
                        String password = jsonObject.getString("password");
                        String email = jsonObject.getString("email");
                        String fullName = jsonObject.getString("fullName");
                        User user = new User(id, username, password, email, fullName);
                        userDao.insertOrReplace(user);
                        finishSignIn(id);
                    } catch (JSONException e) {
                        switch (responseStr) {
                            case "invalid password":
                                Utils.toast(activity, "The password is incorrect");
                                break;
                            case "not exists":
                                userDao.queryBuilder()
                                        .whereOr(UserDao.Properties.Username.eq(username), UserDao.Properties.Email.eq(username)).buildDelete();
                                if (isGoogleSignIn) {
                                    progressBar.setVisibility(View.VISIBLE);
                                    registerGoogleAccount(user);
                                } else {
                                    Utils.toast(activity, "This account does not exists");
                                }
                                break;
                            default:
                                Utils.toast(activity, "Sign in failed");
                                break;
                        }
                    }
                } else {
                    Utils.toast(activity, "Sign in failed");
                }
            }

        });
    }

    private void registerGoogleAccount(final User user) {
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("username", "")
                .add("password", user.getPassword())
                .add("email", user.getEmail())
                .add("fullName", user.getFullName())
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/register.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                progressBar.setVisibility(View.GONE);
                Utils.toast(activity, "Google account registration to app error");
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && responseStr.matches("^userId:[0-9]{1,10}")) {
                    long id = Long.parseLong(responseStr.split(":")[1]);
                    finishSignIn(id);
                } else {
                    Utils.toast(activity, "Google account registration to app failed");
                }
            }
        });
    }

}
