package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ClusterImagesFragment extends BottomSheetDialogFragment {

    private Context context;
    private List<String> imagePaths;

    public static ClusterImagesFragment newInstance(List<String> imagePaths) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("imagePaths", (ArrayList<String>) imagePaths);
        ClusterImagesFragment fragment = new ClusterImagesFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.fragment_cluster_images, null);
        dialog.setContentView(view);
        GridView gridView = view.findViewById(R.id.gridView);
        view.findViewById(R.id.btnClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        imagePaths = getArguments().getStringArrayList("imagePaths");
        ImageAdapter adapter = new ImageAdapter(imagePaths, context, inflater);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String imageName = new File(imagePaths.get(i)).getName();
                Intent intent = new Intent(context, GalleryActivity.class);
                intent.putExtra("imageName", imageName);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }


    private static class ImageAdapter extends BaseAdapter {

        private List<String> list;
        private Context context;
        private LayoutInflater inflater;

        ImageAdapter(List<String> list, Context context, LayoutInflater inflater) {
            this.list = list;
            this.context = context;
            this.inflater = inflater;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public String getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder vh;
            if (view == null) {
                view = inflater.inflate(R.layout.custom_marker, viewGroup, false);
                vh = new ViewHolder(view);
                view.setTag(vh);
            } else {
                vh = (ViewHolder) view.getTag();
            }
            String imagePath = getItem(i);
            File file = new File(imagePath);
            String imageName = file.getName();
            if (file.exists()) {
                vh.txtImageName.setText(imageName);
                GlideApp.with(context)
                        .load(file)
                        .override(120)
                        .centerCrop()
                        .into(vh.imgMarkerImage);
            }
            return view;
        }

        private static class ViewHolder {
            TextView txtImageName;
            ImageView imgMarkerImage;

            ViewHolder(View v) {
                txtImageName = v.findViewById(R.id.txtImageName);
                imgMarkerImage = v.findViewById(R.id.imgMarkerImage);
            }
        }
    }
}
