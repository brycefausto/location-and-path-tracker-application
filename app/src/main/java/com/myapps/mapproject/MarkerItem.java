package com.myapps.mapproject;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MarkerItem implements ClusterItem {

    private LatLng position;
    private String title, snippet;
    private String imageName, imagePath;

    public MarkerItem(LatLng position, String title) {
        this.position = position;
        this.title = title;
    }

    public MarkerItem(LatLng position, String imageName, String imagePath) {
        this.position = position;
        this.imageName = imageName;
        this.imagePath = imagePath;
        snippet = "View image";
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImagePath() {
        return imagePath;
    }
}
