package com.myapps.mapproject;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;
import android.util.LongSparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.MarkerView;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.Cluster;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.ClusterManagerPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("ALL")
public class MapBoxFragment extends MapFragment implements OnMapReadyCallback,
        MapboxMap.OnMapClickListener, MapboxMap.OnMapLongClickListener, MapboxMap.OnPolylineClickListener,
        ClusterManagerPlugin.OnClusterInfoWindowClickListener<MyItem>, ClusterManagerPlugin.OnClusterItemInfoWindowClickListener<MyItem> {

    private Activity activity;
    private MapboxMap map;
    private Marker marker;
    private MarkerView locationMarker;
    private Polyline trackingPolyline, selPolyline;
    private List<LatLng> trackingPoints = new ArrayList<>();
    private LongSparseArray<Polyline> polylineArray = new LongSparseArray<>();
    private MapState mapState;
    private MapFragmentListener listener;
    private List<String> imageNames = new ArrayList<>();
    private ClusterManagerPlugin<MyItem> clusterManagerPlugin;
    private boolean isMapBox;

    public MapBoxFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        return inflater.inflate(R.layout.fragment_map_box, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
        mapState = MapState.getInstance();
        isMapBox = mapState.isMapBox;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isMapBox) {
            if (marker != null) {
                mapState.isMarkerInfoShown = marker.isInfoWindowShown();
            } else {
                mapState.markerPosition = null;
                mapState.markerTitle = null;
                mapState.markerImagePath = null;
            }
            if (map != null) {
                CameraPosition cameraPosition = map.getCameraPosition();
                LatLng target = cameraPosition.target;
                mapState.cameraTarget = new Point(target.getLatitude(), target.getLongitude());
                mapState.setCameraZoom1(cameraPosition.zoom);
                mapState.cameraBearing = (float) cameraPosition.bearing;
                mapState.cameraTilt = (float) cameraPosition.tilt;
            }
        }
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        map = mapboxMap;
        //Set load map state to map
        Point cameraTarget = mapState.cameraTarget;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(cameraTarget.latitude, cameraTarget.longitude))
                .zoom(mapState.cameraZoom1)
                .bearing(mapState.cameraBearing)
                .tilt(mapState.cameraTilt)
                .build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (isMapBox) {
            loadMapType(mapState.mapType);
            UiSettings uiSettings = map.getUiSettings();
            uiSettings.setAttributionEnabled(false);
            uiSettings.setZoomControlsEnabled(true);
            uiSettings.setCompassGravity(Gravity.TOP | Gravity.START);
            map.addOnMapClickListener(this);
            map.addOnMapLongClickListener(this);
            map.setOnPolylineClickListener(this);
            clusterManagerPlugin = new ClusterManagerPlugin<>(activity, map);
            MyItemRenderer myItemRenderer = new MyItemRenderer(activity, map, clusterManagerPlugin);
            clusterManagerPlugin.setRenderer(myItemRenderer);
            clusterManagerPlugin.setOnClusterInfoWindowClickListener(this);
            clusterManagerPlugin.setOnClusterItemInfoWindowClickListener(this);
            map.addOnCameraIdleListener(clusterManagerPlugin);
            map.setOnMarkerClickListener(clusterManagerPlugin);
            map.setOnInfoWindowClickListener(clusterManagerPlugin);
            Point markerPosition = mapState.markerPosition;
            final String imagePath = mapState.markerImagePath;
            if (markerPosition != null) {
                marker = map.addMarker(new MarkerOptions().position(
                        new LatLng(markerPosition.latitude, markerPosition.longitude))
                        .setTitle(mapState.markerTitle));
            }
            if (mapState.locationPosition != null && (Settings.isShowBlueDot() || TrackingService.isServiceRunning)) {
                Point locationPosition = mapState.locationPosition;
                setLocationMarker(locationPosition.latitude, locationPosition.longitude);
            }
            if (imagePath != null && !imagePath.equals("") && !mapState.isImagesShown) {
                File file = new File(imagePath);
                final String imageName = file.getName();
                if (file.exists() && !imageNames.contains(imageName)) {
                    myItemRenderer.createImageMarker(marker, imageName, imagePath);
                }
            }
            if (mapState.isMarkerInfoShown && marker != null)
                map.selectMarker(marker);
            List<LatLng> pathPoints = pointsToLatLngList(TrackingService.getPathPoints());
            if (pathPoints.size() > 0) {
                trackingPolyline = addPolyline(pathPoints);
                trackingPolyline.setColor(Color.BLUE);
            }
            if (mapState.isPathsShown && !TrackingService.isServiceRunning)
                showAllPaths();
            if (mapState.isImagesShown)
                showAllImages();
            long selPathId = mapState.selPathId;
            if (selPathId > 0) {
                Polyline polyline = polylineArray.get(selPathId);
                if (polyline == null) {
                    Path path = PathsData.getPath(selPathId);
                    if (path != null) {
                        polyline = addPolyline(pointsToLatLngList(path.getPointsLatLong()));
                        polyline.setId(path.getId());
                        polylineArray.put(path.getId(), polyline);
                    }
                }
                LatLng polylineStart = polyline.getPoints().get(0);
                if (polylineStart != null) {
                    map.animateCamera(CameraUpdateFactory.newLatLng(polylineStart));
                    selectPath(polyline);
                }
            }
        }
    }

    private LatLng pointToLatLng(Point point) {
        return new LatLng(point.latitude, point.longitude);
    }

    private List<LatLng> pointsToLatLngList(List<Point> points) {
        List<LatLng> latLngList = new ArrayList<>();
        for (Point point : points) {
            latLngList.add(pointToLatLng(point));
        }
        return latLngList;
    }

    private void addMarkerImage(LatLng position, String imageName, String imagePath) {
        clusterManagerPlugin.addItem(new MyItem(position, imageName, imagePath));
        imageNames.add(imageName);
    }

    @Override
    public void showAllPaths() {
        LongSparseArray<Path> savedPathsArray = PathsData.getSavedPathsArray();
        for (int i = 0; i < savedPathsArray.size(); i++) {
            Path path = savedPathsArray.valueAt(i);
            if (polylineArray.get(path.getId()) == null) {
                List<LatLng> latLngList = pointsToLatLngList(path.getPointsLatLong());
                Polyline polyline = addPolyline(latLngList);
                polyline.setPoints(latLngList);
                polyline.setId(path.getId());
                polylineArray.put(path.getId(), polyline);
            }
        }
    }

    private Polyline addPolyline(List<LatLng> latLngList) {
        return map.addPolyline(new PolylineOptions()
                .width(4)
                .addAll(latLngList));
    }

    @Override
    public void hideAllPaths() {
        for (int i = 0; i < polylineArray.size(); i++) {
            Polyline polyline = polylineArray.valueAt(i);
            polyline.remove();
        }
        polylineArray.clear();
    }

    @Override
    public void showAllImages() {
        File mediaStorageDir = Utils.getPicturesDirectory();
        List<Picture> pictures = new ArrayList<>();
        List<File> files = Arrays.asList(mediaStorageDir.listFiles(GalleryActivity.IMAGE_FILTER));
        for (File file : files) {
            try {
                String imagePath = file.getAbsolutePath();
                ExifInterface exif = new ExifInterface(imagePath);
                double[] latLong = exif.getLatLong();
                if (latLong != null) {
                    Picture picture = new Picture(imagePath, file.getName());
                    picture.setLatLong(latLong[0], latLong[1]);
                    pictures.add(picture);
                }
            } catch (IOException ignored) {
            }
        }
        for (Picture picture : pictures) {
            double[] latLong = picture.getLatLong();
            for (Picture picture1 : pictures) {
                if (picture1 != picture) {
                    double[] latLong1 = picture1.getLatLong();
                    if (latLong[0] == latLong1[0] && latLong[1] == latLong1[1]) {
                        latLong1[1] += 0.00001;
                        picture1.setLatLong(latLong1[0], latLong1[1]);
                    }
                }
            }
        }
        if (marker != null) marker.remove();
        for (Picture picture : pictures) {
            double[] latLong = picture.getLatLong();
            addMarkerImage(new LatLng(latLong[0], latLong[1]), picture.getName(), picture.getPath());
        }
        clusterManagerPlugin.cluster();
    }

    @Override
    public void hideAllImages() {
        clusterManagerPlugin.clearItems();
        clusterManagerPlugin.cluster();
        imageNames.clear();
    }

    @Override
    public void setListener(MapFragmentListener listener) {
        this.listener = listener;
    }

    private void selectPath(Polyline polyline) {
        deselectPath();
        selPolyline = polyline;
        if (polyline != null) {
            polyline.setColor(Color.BLUE);
            long pathId = polyline.getId();
            mapState.selPathId = pathId;
            if (listener != null) listener.onPathSelect(pathId);
        }
    }

    @Override
    public void deselectPath() {
        if (selPolyline != null) {
            selPolyline.setColor(Color.BLACK);
            selPolyline = null;
        }
    }

    private void deselectPath1() {
        deselectPath();
        if (listener != null) listener.onPathDeselect();
    }

    @Override
    public void deleteSelectedPath() {
        if (selPolyline != null) {
            selPolyline.remove();
        }
    }

    @Override
    public void trackPath(Location location) {
        LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        moveCameraRotate(position, location.getBearing());
        setLocationMarker(position.getLatitude(), position.getLongitude());
        if (trackingPoints.size() == 0) {
            for (Point point : TrackingService.getPathPoints()) {
                trackingPoints.add(new LatLng(point.latitude, point.longitude));
            }
        }
        trackingPoints.add(position);
        if (trackingPolyline == null) {
            trackingPolyline = addPolyline(new ArrayList<LatLng>());
            trackingPolyline.setColor(Color.BLUE);
            hideAllPaths();
            hideAllImages();
        }
        trackingPolyline.setPoints(trackingPoints);
    }

    @Override
    public void endTrack() {
        if (mapState.isPathsShown)
            showAllPaths();
        if (mapState.isImagesShown)
            showAllImages();
    }

    @Override
    public void savePath(PathDao pathDao, long userId, String pathName) {
        if (trackingPolyline != null) {
            trackingPolyline.setColor(Color.BLACK);
            List<Point> pathPoints = new ArrayList<>();
            for (LatLng latLng : trackingPolyline.getPoints()) {
                pathPoints.add(new Point(latLng.getLatitude(), latLng.getLongitude()));
            }
            long pathId = PathsData.savePath(pathDao, userId, pathName, pathPoints);
            polylineArray.put(pathId, trackingPolyline);
            trackingPolyline.remove();
            trackingPolyline = null;
        }
    }

    @Override
    public void deletePath() {
        if (trackingPolyline != null) {
            trackingPolyline.remove();
            trackingPolyline = null;
            trackingPoints.clear();
        }
    }

    @Override
    public void setMarker(double latitude, double longitude, String title) {
        if (marker != null) marker.remove();
        mapState.markerPosition = new Point(latitude, longitude);
        mapState.markerTitle = title;
        mapState.markerImagePath = "";
        marker = map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(title));
    }

    private void moveCameraRotate(LatLng position, float bearing) {
        CameraPosition cameraPosition = map.getCameraPosition();
        double zoom = cameraPosition.zoom;
        if (cameraPosition.zoom < 20)
            zoom = 20;
        if (!Settings.isTrackFacing()) bearing = 0;
        cameraPosition = new CameraPosition.Builder(map.getCameraPosition())
                .target(position)
                .zoom(zoom)
                .bearing(bearing)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void setLocationMarker(double latitude, double longitude) {
        LatLng position = new LatLng(latitude, longitude);
        mapState.locationPosition = new Point(latitude, longitude);
        if (locationMarker == null) {
            locationMarker = map.addMarker(new MarkerViewOptions()
                    .position(position)
                    .icon(IconFactory.getInstance(activity).fromResource(R.drawable.location))
                    .flat(true));
        } else {
            MarkerAnimationMB.animateMarker(locationMarker, position, new MarkerAnimationMB.LatLngInterpolator.Linear());
        }
    }

    @Override
    public void setLocation(double latitude, double longitude) {
        double zoom = map.getCameraPosition().zoom;
        if (zoom < 15)
            zoom = 15;
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoom));
        setLocationMarker(latitude, longitude);
    }

    @Override
    public void moveCamera(double latitude, double longitude, float zoom, float bearing, float tilt) {
        if (map != null) {
            if (!Settings.isTrackFacing()) bearing = 0;
            CameraPosition cameraPosition = new CameraPosition.Builder(map.getCameraPosition())
                    .target(new LatLng(latitude, longitude))
                    .zoom(zoom)
                    .bearing(bearing)
                    .build();
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void setType(int type) {
        if (type != mapState.mapType) {
            mapState.mapType = type;
            loadMapType(type);
        }
    }

    private void loadMapType(int type) {
        if (map != null) {
            String style = Style.MAPBOX_STREETS;
            switch (type) {
                case MapState.TYPE_SATELLITE:
                    style = Style.SATELLITE;
                    break;
                case MapState.TYPE_HYBRID:
                    style = Style.SATELLITE_STREETS;
                    break;
            }
            map.setStyle(style);
        }
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {
        if (!TrackingService.isServiceRunning && marker != null)
            marker.remove();
        deselectPath1();
    }

    @Override
    public void onMapLongClick(@NonNull LatLng point) {
        CameraPosition cameraPosition = new CameraPosition.Builder(map.getCameraPosition())
                .target(point)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        setMarker(point.getLatitude(), point.getLongitude(), "Marked location");
    }

    @Override
    public void onPolylineClick(@NonNull Polyline polyline) {
        if (!TrackingService.isServiceRunning) {
            selectPath(polyline);
            mapState.selPathId = polyline.getId();
        }
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MyItem> cluster) {
        List<String> imagePaths = new ArrayList<>();
        for (MyItem item : cluster.getItems()) {
            imagePaths.add(item.getImagePath());
        }
        ClusterImagesFragment fragment = ClusterImagesFragment.newInstance(imagePaths);
        fragment.show(getChildFragmentManager(), fragment.getTag());
    }

    @Override
    public void onClusterItemInfoWindowClick(MyItem item) {
        String imageName = item.getImageName();
        if (imageName != null) {
            Intent intent = new Intent(activity, GalleryActivity.class);
            intent.putExtra("imageName", imageName);
            activity.startActivity(intent);
        }
    }
}
