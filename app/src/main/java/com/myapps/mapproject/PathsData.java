package com.myapps.mapproject;

import android.util.LongSparseArray;

import org.joda.time.DateTime;

import java.util.List;

public class PathsData {

    private static LongSparseArray<Path> savedPathsArray;

    public PathsData() {
    }

    static LongSparseArray<Path> getSavedPathsArray() {
        if (savedPathsArray == null)
            savedPathsArray = new LongSparseArray<>();
        return savedPathsArray;
    }

    static long savePath(PathDao pathDao, long userId, String pathName, List<Point> pathPoints) {
        String dateTime = new DateTime().toString();
        Path path = new Path();
        path.setUserId(userId);
        path.setPathName(pathName);
        path.setDateTime(dateTime);
        path.setPoints(pathPoints);
        long id = pathDao.insert(path);
        getSavedPathsArray().put(id, new Path(id, userId, pathName, dateTime, pathPoints));
        return id;
    }

    static void renamePath(PathDao pathDao, long id, String rename) {
        getSavedPathsArray().get(id).setPathName(rename);
        Path path = pathDao.queryBuilder()
                .where(PathDao.Properties.Id.eq(id))
                .unique();
        if (path != null) {
            path.setPathName(rename);
            pathDao.update(path);
        }
    }

    static Path getPath(long id) {
        return getSavedPathsArray().get(id);
    }

    static void deletePath(PathDao pathDao, long pathId) {
        getSavedPathsArray().delete(pathId);
        pathDao.deleteByKey(pathId);
    }

    static void load(PathDao pathDao) {
        if (savedPathsArray == null) {
            List<Path> paths = pathDao.queryBuilder().list();
            for (Path path : paths) {
                getSavedPathsArray().put(path.getId(), path);
            }
        }
    }

    static void removeSavedPaths() {
        savedPathsArray = null;
    }

}
