package com.myapps.mapproject;

import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MapSelectorActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnCameraIdleListener {

    private static SparseArrayCompat<Integer> mapSizes = new SparseArrayCompat<Integer>() {{
        put(15, 70);
        put(14, 70);
        put(13, 72);
        put(12, 73);
        put(11, 78);
        put(10, 94);
    }};

    private MapState mapState;
    private MapboxMap map;
    private OfflineTilePyramidRegionDefinition definition;
    private View progressDisplay;
    private ProgressBar progressBar;
    private TextView txtProgress, txtMapSize;
    private LongSparseArray<OfflineRegion> offlineRegionArray = new LongSparseArray<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_selector);
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        progressDisplay = findViewById(R.id.progressDisplay);
        progressBar = findViewById(R.id.progressBar);
        txtProgress = findViewById(R.id.txtProgress);
        txtMapSize = findViewById(R.id.txtMapSize);
        if (MapDownloadService.isRunning)
            progressDisplay.setVisibility(View.VISIBLE);
        mapState = MapState.getInstance();
        OfflineManager offlineManager = OfflineManager.getInstance(this);
        offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
            @Override
            public void onList(OfflineRegion[] offlineRegions) {
                for (OfflineRegion offlineRegion : offlineRegions) {

                    offlineRegionArray.put(offlineRegion.getID(), offlineRegion);
                }
            }

            @Override
            public void onError(String error) {

            }
        });
        Utils.checkInternetWithAlert(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (map != null) {
            CameraPosition cameraPosition = map.getCameraPosition();
            LatLng target = cameraPosition.target;
            mapState.cameraTarget = new Point(target.getLatitude(), target.getLongitude());
            mapState.setCameraZoom1(cameraPosition.zoom);
            mapState.cameraBearing = (float) cameraPosition.bearing;
            mapState.cameraTilt = (float) cameraPosition.tilt;
        }
    }

    public void onCloseClick(View v) {
        finish();
    }

    public void onDownloadClick(View v) {
        if (Utils.checkInternetWithAlert(this) && !MapDownloadService.isRunning)
            nameOfflineMap();
    }

    public void nameOfflineMap() {
        int mapNum = offlineRegionArray.size() + 1;
        String mapName = "Map " + mapNum;
        Utils.showNameDialog(this, "Download this map", mapName, new Utils.NameDialogCallback() {
            @Override
            public void onPositiveClick(AlertDialog dialog, String name) {
                downloadMap(name);
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(AlertDialog dialog) {
            }
        });
    }

    private void downloadMap(final String mapName) {
        progressDisplay.setVisibility(View.VISIBLE);
        byte[] metadata = OfflineUtils.convertRegionName(mapName);
        MapDownloadService.startDownload(this, definition, metadata);
        Utils.toast(this, "Starting download");
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        map = mapboxMap;
        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setAttributionEnabled(false);
        uiSettings.setZoomControlsEnabled(true);
        map.addOnCameraIdleListener(this);
        map.setMinZoomPreference(10);
        map.setMaxZoomPreference(15);
        Point cameraTarget = mapState.cameraTarget;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(cameraTarget.latitude, cameraTarget.longitude))
                .zoom(mapState.cameraZoom1)
                .bearing(mapState.cameraBearing)
                .tilt(mapState.cameraTilt)
                .build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onCameraIdle() {
        loadMapSize();
    }

    private void loadMapSize() {
        Double zoom = map.getCameraPosition().zoom;
        LatLngBounds latLngBounds = map.getProjection().getVisibleRegion().latLngBounds;
        definition = new OfflineTilePyramidRegionDefinition(
                map.getStyleUrl(),
                latLngBounds,
                zoom,
                20,
                getResources().getDisplayMetrics().density
        );
        int zoom1 = (int) Math.round(zoom);
        int mapSize = mapSizes.get(zoom1);
        if (mapSize != 0) {
            String mapSizeText = "Download up to " + mapSize + " MB";
            txtMapSize.setText(mapSizeText);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventDownload(MapDownloadService.DownloadEvent event) {
        switch (event.type) {
            case MapDownloadService.EVENT_PROGRESS:
                progressBar.setProgress(event.progress);
                String progressText = "Downloading map: " + event.progress + "%";
                txtProgress.setText(progressText);
                break;
            case MapDownloadService.EVENT_COMPLETE:
                progressDisplay.setVisibility(View.GONE);
                break;
            case MapDownloadService.EVENT_CANCELED:
            case MapDownloadService.EVENT_ERROR:
                progressDisplay.setVisibility(View.GONE);
                break;
        }
    }

}
