package com.myapps.mapproject;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import com.mapbox.mapboxsdk.Mapbox;

import org.greenrobot.greendao.database.Database;

public class MapApp extends Application implements Application.ActivityLifecycleCallbacks {

    public final static String APP_DATABASE_NAME = "app_data.db";

    private static int activityCounter = 0;
    private DaoSession daoSession;

    public static boolean isAppBackground() {
        return activityCounter == 0;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
        Database db = new DaoMaster.DevOpenHelper(getBaseContext(), APP_DATABASE_NAME).getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        Mapbox.getInstance(getApplicationContext(), getString(R.string.mapbox_key));
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        activityCounter++;
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        activityCounter--;
        if (activityCounter == 0) {
            MapState.removeInstance();
            PathsData.removeSavedPaths();
        }
        startService(new Intent(this, AppDataService.class));
    }
}
