package com.myapps.mapproject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppDataService extends Service {

    public final static String ACTION_IMPORT = "IMPORT";

    public static boolean isRunning;

    private final Context context = this;

    private PathDao pathDao;
    private OfflineManager offlineManager;
    private long userId;

    public AppDataService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
        DaoSession daoSession = ((MapApp) getApplication()).getDaoSession();
        pathDao = daoSession.getPathDao();
        offlineManager = OfflineManager.getInstance(this);
        userId = User.getUserId(Utils.getSharedPreferences(this));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (action == null)
                exportAppData();
            else if (action.equals(ACTION_IMPORT)) {
                importAppData();
            }
            return START_STICKY;
        }
        return START_NOT_STICKY;
    }

    private void exportAppData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //Directory for the data files
                final File dir = new File(getExternalFilesDir(null), "data_files");
                if (!dir.exists() && !dir.mkdir()) {
                    stopSelf();
                    return;
                }
                try {
                    final Gson gson = new Gson();
                    List<Path> paths = pathDao.queryBuilder().where(PathDao.Properties.UserId.eq(userId)).list();
                    String pathsJson = gson.toJson(paths);
                    String userPathsPath = dir.getPath() + File.separator + "user_paths.dat";
                    Utils.writeToFile(userPathsPath, pathsJson);
                    offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
                        @Override
                        public void onList(OfflineRegion[] offlineRegions) {
                            List<MapData> mapDataList = new ArrayList<>();
                            for (OfflineRegion offlineRegion : offlineRegions) {
                                MapData mapData = MapData.offlineRegionToMapData(offlineRegion);
                                if (mapData != null)
                                    mapDataList.add(mapData);
                            }
                            String mapsJson = gson.toJson(mapDataList);
                            String offlineMapsPath = dir.getPath() + File.separator + "offline_maps.dat";
                            Utils.writeToFile(offlineMapsPath, mapsJson);
                            stopSelf();
                        }

                        @Override
                        public void onError(String error) {
                            stopSelf();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                stopSelf();
            }
        }).start();
    }

    private void importAppData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //Directory for the data files
                final File dir = new File(getExternalFilesDir(null), "data_files");
                if (!dir.exists()) {
                    stopSelf();
                    return;
                }
                String userPathsPath = dir.getPath() + File.separator + "user_paths.dat";
                String offlineMapsPath = dir.getPath() + File.separator + "offline_maps.dat";
                File userPathsFile = new File(userPathsPath);
                File offlineMapsFile = new File(offlineMapsPath);
                if (!userPathsFile.exists() || !offlineMapsFile.exists()) {
                    stopSelf();
                    return;
                }
                try {
                    final Gson gson = new Gson();
                    String pathData = Utils.readFile(userPathsFile);
                    String offlineMaps = Utils.readFile(offlineMapsFile);
                    List<Path> paths = gson.fromJson(pathData, new TypeToken<List<Path>>() {
                    }.getType());
                    for (Path path : paths) {
                        Path path1 = pathDao.queryBuilder()
                                .where(PathDao.Properties.DateTime.eq(path.getDateTime()), PathDao.Properties.UserId.eq(path.getUserId()))
                                .unique();
                        if (path1 == null) {
                            path.setId(null);
                            pathDao.insert(path);
                        }
                    }
                    final List<MapData> mapDataList = gson.fromJson(offlineMaps, new TypeToken<List<MapData>>() {
                    }.getType());
                    offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
                        @Override
                        public void onList(OfflineRegion[] offlineRegions) {
                            for (OfflineRegion offlineRegion : offlineRegions) {
                                MapData mapData1 = MapData.offlineRegionToMapData(offlineRegion);
                                if (mapData1 != null) {
                                    for (Iterator<MapData> iterator = mapDataList.iterator(); iterator.hasNext(); ) {
                                        MapData mapData = iterator.next();
                                        if (mapData.mapName.equals(mapData1.mapName) &&
                                                mapData.mapDate.equals(mapData1.mapDate) &&
                                                mapData.mapSize == mapData1.mapSize) {
                                            iterator.remove();
                                        }
                                    }
                                }
                            }
                            for (MapData mapData : mapDataList) {
                                byte[] regionName = OfflineUtils.convertRegionName(mapData.mapName);
                                MapDownloadService.startDownload(context, mapData.definition, regionName);
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }
}
