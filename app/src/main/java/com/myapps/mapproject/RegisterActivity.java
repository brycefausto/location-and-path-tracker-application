package com.myapps.mapproject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    private final Activity activity = this;

    private EditText editUsername, editPassword, editRetypePassword, editEmail, editFullName;
    private ProgressBar progressBar;
    private UserDao userDao;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        editRetypePassword = findViewById(R.id.editRetypePassword);
        editEmail = findViewById(R.id.editEmail);
        editFullName = findViewById(R.id.editFullName);
        progressBar = findViewById(R.id.progressBar);
        DaoSession daoSession = ((MapApp) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();
        pref = getSharedPreferences("pref", MODE_PRIVATE);
    }

    public void onRegisterClick(View v) {
        if (!Utils.isNetworkAvailable(this)) {
            Utils.showInternetAlert(this);
            return;
        }
        String username = editUsername.getText().toString().toLowerCase();
        String password = editPassword.getText().toString();
        String email = editEmail.getText().toString().toLowerCase();
        String retypePassword = editRetypePassword.getText().toString();
        String fullName = editFullName.getText().toString();
        if (!(Utils.isValidUsername(this, username) && Utils.isValidPasswords(this, password, retypePassword) &&
                Utils.isValidEmail(this, email) && Utils.isValidFullName(this, fullName))) {
            return;
        }
        User user = new User(username, password, email, fullName);
        User user1 = userDao.queryBuilder()
                .whereOr(UserDao.Properties.Username.eq(username), UserDao.Properties.Email.eq(email))
                .unique();
        if (user1 != null) {
            checkAccount(user, user1);
            return;
        }
        registerOnline(user);
    }

    private void checkAccount(final User newUser, final User existingUser) {
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("username", existingUser.getUsername())
                .add("email", existingUser.getEmail())
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/check_account.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                progressBar.setVisibility(View.GONE);
                Utils.toast(activity, "Registration error");
                Utils.checkInternetWithAlert(activity);
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    switch (responseStr) {
                        case "exists":
                            Utils.toast(activity, "The username/email is already existing in accounts");
                            break;
                        case "not exists":
                            userDao.queryBuilder()
                                    .whereOr(UserDao.Properties.Username.eq(existingUser.getUsername()), UserDao.Properties.Email.eq(existingUser.getEmail()))
                                    .buildDelete();
                            registerOnline(newUser);
                            break;
                        default:
                            Utils.toast(activity, "Registration error");
                    }
                }
            }
        });
    }

    private void registerOnline(final User user) {
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("username", user.getUsername())
                .add("password", user.getPassword())
                .add("email", user.getEmail())
                .add("fullName", user.getFullName())
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/register.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                progressBar.setVisibility(View.GONE);
                Utils.toast(activity, "Registration error");
                Utils.checkInternetWithAlert(activity);
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && responseStr.matches("^userId:[0-9]{1,10}")) {
                    long id = Long.parseLong(responseStr.split(":")[1]);
                    user.setId(id);
                    userDao.insert(user);
                    User.saveSignIn(id, pref);
                    Intent intent = new Intent(activity, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else if (responseStr.equals("user exists")) {
                    Utils.toast(activity, "The username/email is already existing in accounts");
                } else {
                    Utils.toast(activity, "Registration failed");
                }
            }
        });

    }
}
