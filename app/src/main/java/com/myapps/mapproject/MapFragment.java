package com.myapps.mapproject;

import android.location.Location;
import android.support.v4.app.Fragment;

public abstract class MapFragment extends Fragment {

    public abstract void showAllPaths();

    public abstract void hideAllPaths();

    public abstract void showAllImages();

    public abstract void hideAllImages();

    public abstract void setListener(MapFragmentListener listener);

    public abstract void deselectPath();

    public abstract void deleteSelectedPath();

    public abstract void trackPath(Location location);

    public abstract void endTrack();

    public abstract void savePath(PathDao pathDao, long userId, String pathName);

    public abstract void deletePath();

    public abstract void setMarker(double latitude, double longitude, String title);

    public abstract void setLocationMarker(double latitude, double longitude);

    public abstract void setLocation(double latitude, double longitude);

    public abstract void moveCamera(double latitude, double longitude, float zoom, float bearing, float tilt);

    public abstract void setType(int type);

    public interface MapFragmentListener {
        void onPathSelect(long pathId);

        void onPathDeselect();
    }

}
