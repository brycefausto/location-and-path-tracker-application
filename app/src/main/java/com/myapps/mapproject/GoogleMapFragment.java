package com.myapps.mapproject;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;
import android.util.LongSparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GoogleMapFragment extends MapFragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnPolylineClickListener,
        ClusterManager.OnClusterInfoWindowClickListener<MarkerItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MarkerItem> {

    private Activity activity;
    private GoogleMap map;
    private Marker marker, locationMarker;
    private Polyline trackingPolyline, selPolyline;
    private List<LatLng> trackingPoints = new ArrayList<>();
    private LongSparseArray<Polyline> polylineArray = new LongSparseArray<>();
    private PolylineOptions polylineOptions;
    private MapState mapState;
    private MapFragmentListener listener;
    private List<String> imageNames = new ArrayList<>();
    private ClusterManager<MarkerItem> clusterManager;
    private boolean isGoogleMap;

    public GoogleMapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        return inflater.inflate(R.layout.fragment_google_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
        mapState = MapState.getInstance();
        isGoogleMap = !mapState.isMapBox;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isGoogleMap) {
            if (marker != null) {
                mapState.isMarkerInfoShown = marker.isInfoWindowShown();
            } else {
                mapState.markerPosition = null;
                mapState.markerTitle = null;
                mapState.markerImagePath = null;
            }
            if (map != null) {
                CameraPosition cameraPosition = map.getCameraPosition();
                LatLng target = cameraPosition.target;
                mapState.cameraTarget = new Point(target.latitude, target.longitude);
                mapState.setCameraZoom(cameraPosition.zoom);
                mapState.cameraBearing = cameraPosition.bearing;
                mapState.cameraTilt = cameraPosition.tilt;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //Set load map state to map
        Point cameraTarget = mapState.cameraTarget;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(cameraTarget.latitude, cameraTarget.longitude))
                .zoom(mapState.cameraZoom)
                .bearing(mapState.cameraBearing)
                .tilt(mapState.cameraTilt)
                .build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (isGoogleMap) {
            loadMapType(mapState.mapType);
            UiSettings uiSettings = map.getUiSettings();
            uiSettings.setZoomControlsEnabled(true);
            uiSettings.setIndoorLevelPickerEnabled(true);
            map.setBuildingsEnabled(true);
            map.setIndoorEnabled(true);
            map.setOnMapClickListener(this);
            map.setOnMapLongClickListener(this);
            map.setOnPolylineClickListener(this);
            clusterManager = new ClusterManager<>(activity, map);
            MarkerItemRenderer markerItemRenderer = new MarkerItemRenderer(activity, map, clusterManager);
            clusterManager.setRenderer(markerItemRenderer);
            clusterManager.setOnClusterInfoWindowClickListener(this);
            clusterManager.setOnClusterItemInfoWindowClickListener(this);
            map.setOnCameraIdleListener(clusterManager);
            map.setOnMarkerClickListener(clusterManager);
            map.setOnInfoWindowClickListener(clusterManager);
            Point markerPosition = mapState.markerPosition;
            final String imagePath = mapState.markerImagePath;
            if (markerPosition != null) {
                marker = map.addMarker(new MarkerOptions().position(
                        new LatLng(markerPosition.latitude, markerPosition.longitude))
                        .title(mapState.markerTitle));
            }
            if (mapState.locationPosition != null && (Settings.isShowBlueDot() || TrackingService.isServiceRunning)) {
                Point locationPosition = mapState.locationPosition;
                setLocationMarker(locationPosition.latitude, locationPosition.longitude);
            }
            polylineOptions = new PolylineOptions().width(6).jointType(JointType.ROUND).geodesic(true).clickable(true);
            if (imagePath != null && !imagePath.equals("") && !mapState.isImagesShown) {
                File file = new File(imagePath);
                final String imageName = file.getName();
                if (file.exists() && !imageNames.contains(imageName)) {
                    markerItemRenderer.createImageMarker(marker, imageName, imagePath);
                }
            }
            if (mapState.isMarkerInfoShown && marker != null)
                marker.showInfoWindow();
            //Load tracking path
            List<LatLng> pathPoints = pointsToLatLngList(TrackingService.getPathPoints());
            if (pathPoints.size() > 0) {
                trackingPolyline = map.addPolyline(polylineOptions);
                trackingPolyline.setPoints(pathPoints);
                trackingPolyline.setColor(Color.BLUE);
            }
            if (mapState.isPathsShown && !TrackingService.isServiceRunning)
                showAllPaths();
            if (mapState.isImagesShown)
                showAllImages();
            long selPathId = mapState.selPathId;
            if (selPathId > 0) {
                Polyline polyline = polylineArray.get(selPathId);
                if (polyline == null) {
                    Path path = PathsData.getPath(selPathId);
                    if (path != null) {
                        polyline = map.addPolyline(polylineOptions);
                        polyline.setPoints(pointsToLatLngList(path.getPointsLatLong()));
                        polyline.setTag(path.getId());
                        polylineArray.put(path.getId(), polyline);
                    }
                }
                LatLng polylineStart = polyline.getPoints().get(0);
                if (polylineStart != null) {
                    map.animateCamera(CameraUpdateFactory.newLatLng(polylineStart));
                    selectPath(polyline);
                }
            }
        }
    }

    private LatLng pointToLatLng(Point point) {
        return new LatLng(point.latitude, point.longitude);
    }

    private List<LatLng> pointsToLatLngList(List<Point> points) {
        List<LatLng> latLngList = new ArrayList<>();
        for (Point point : points) {
            latLngList.add(pointToLatLng(point));
        }
        return latLngList;
    }

    private void addMarkerImage(LatLng position, String imageName, String imagePath) {
        clusterManager.addItem(new MarkerItem(position, imageName, imagePath));
        imageNames.add(imageName);
    }

    @Override
    public void showAllPaths() {
        LongSparseArray<Path> savedPathsArray = PathsData.getSavedPathsArray();
        for (int i = 0; i < savedPathsArray.size(); i++) {
            Path path = savedPathsArray.valueAt(i);
            if (polylineArray.get(path.getId()) == null) {
                Polyline polyline = map.addPolyline(polylineOptions);
                List<LatLng> latLngList = pointsToLatLngList(path.getPointsLatLong());
                polyline.setPoints(latLngList);
                polyline.setTag(path.getId());
                polylineArray.put(path.getId(), polyline);
            }
        }
    }

    @Override
    public void hideAllPaths() {
        for (int i = 0; i < polylineArray.size(); i++) {
            Polyline polyline = polylineArray.valueAt(i);
            polyline.remove();
        }
        polylineArray.clear();
    }

    @Override
    public void showAllImages() {
        File mediaStorageDir = Utils.getPicturesDirectory();
        List<Picture> pictures = new ArrayList<>();
        List<File> files = Arrays.asList(mediaStorageDir.listFiles(GalleryActivity.IMAGE_FILTER));
        for (File file : files) {
            try {
                String imagePath = file.getAbsolutePath();
                ExifInterface exif = new ExifInterface(imagePath);
                double[] latLong = exif.getLatLong();
                if (latLong != null) {
                    Picture picture = new Picture(imagePath, file.getName());
                    picture.setLatLong(latLong[0], latLong[1]);
                    pictures.add(picture);
                }
            } catch (IOException ignored) {
            }
        }
        for (Picture picture : pictures) {
            double[] latLong = picture.getLatLong();
            for (Picture picture1 : pictures) {
                if (picture1 != picture) {
                    double[] latLong1 = picture1.getLatLong();
                    if (latLong[0] == latLong1[0] && latLong[1] == latLong1[1]) {
                        latLong1[1] += 0.00001;
                        picture1.setLatLong(latLong1[0], latLong1[1]);
                    }
                }
            }
        }
        if (marker != null) marker.remove();
        for (Picture picture : pictures) {
            double[] latLong = picture.getLatLong();
            addMarkerImage(new LatLng(latLong[0], latLong[1]), picture.getName(), picture.getPath());
        }
        clusterManager.cluster();
    }

    @Override
    public void hideAllImages() {
        clusterManager.clearItems();
        clusterManager.cluster();
        imageNames.clear();
    }

    @Override
    public void setListener(MapFragmentListener listener) {
        this.listener = listener;
    }

    private void selectPath(Polyline polyline) {
        deselectPath();
        selPolyline = polyline;
        if (polyline != null) {
            polyline.setColor(Color.BLUE);
            long pathId = getPathID(polyline);
            mapState.selPathId = pathId;
            if (listener != null) listener.onPathSelect(pathId);
        }
    }

    @Override
    public void deselectPath() {
        if (selPolyline != null) {
            selPolyline.setColor(Color.BLACK);
            selPolyline = null;
            mapState.selPathId = 0;
        }
    }

    private void deselectPath1() {
        deselectPath();
        if (listener != null) listener.onPathDeselect();
    }

    @Override
    public void deleteSelectedPath() {
        if (selPolyline != null) {
            selPolyline.remove();
        }
    }

    private long getPathID(Polyline polyline) {
        Object object = polyline.getTag();
        if (object instanceof Long) {
            return (long) polyline.getTag();
        }
        return 0;
    }

    @Override
    public void trackPath(Location location) {
        LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        moveCameraRotate(position, location.getBearing());
        setLocationMarker(position.latitude, position.longitude);
        if (trackingPoints.size() == 0) {
            for (Point point : TrackingService.getPathPoints()) {
                trackingPoints.add(new LatLng(point.latitude, point.longitude));
            }
        }
        trackingPoints.add(position);
        if (trackingPolyline == null) {
            trackingPolyline = map.addPolyline(polylineOptions);
            trackingPolyline.setColor(Color.BLUE);
            hideAllPaths();
            hideAllImages();
        }
        trackingPolyline.setPoints(trackingPoints);
    }

    @Override
    public void endTrack() {
        if (mapState.isPathsShown)
            showAllPaths();
        if (mapState.isImagesShown)
            showAllImages();
    }

    @Override
    public void savePath(PathDao pathDao, long userId, String pathName) {
        if (trackingPolyline != null) {
            trackingPolyline.setColor(Color.BLACK);
            List<Point> pathPoints = new ArrayList<>();
            for (LatLng latLng : trackingPolyline.getPoints()) {
                pathPoints.add(new Point(latLng.latitude, latLng.longitude));
            }
            long pathId = PathsData.savePath(pathDao, userId, pathName, pathPoints);
            polylineArray.put(pathId, trackingPolyline);
            trackingPolyline.remove();
            trackingPolyline = null;
        }
    }

    @Override
    public void deletePath() {
        if (trackingPolyline != null) {
            trackingPolyline.remove();
            trackingPolyline = null;
            trackingPoints.clear();
        }
    }

    @Override
    public void setMarker(double latitude, double longitude, String title) {
        if (marker != null) marker.remove();
        mapState.markerPosition = new Point(latitude, longitude);
        mapState.markerTitle = title;
        mapState.markerImagePath = "";
        marker = map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(title));
    }

    private void moveCameraRotate(LatLng position, float bearing) {
        CameraPosition cameraPosition = map.getCameraPosition();
        float zoom = cameraPosition.zoom;
        if (cameraPosition.zoom < 20)
            zoom = 20;
        if (!Settings.isTrackFacing()) bearing = 0;
        cameraPosition = CameraPosition.builder(map.getCameraPosition())
                .target(position)
                .zoom(zoom)
                .bearing(bearing)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void setLocationMarker(double latitude, double longitude) {
        LatLng position = new LatLng(latitude, longitude);
        mapState.locationPosition = new Point(latitude, longitude);
        if (locationMarker == null) {
            locationMarker = map.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
                    .flat(true));
        } else {
            MarkerAnimation.animateMarker(locationMarker, position, new MarkerAnimation.LatLngInterpolator.Linear());
        }
    }

    @Override
    public void setLocation(double latitude, double longitude) {
        float zoom = map.getCameraPosition().zoom;
        if (zoom < 15)
            zoom = 15;
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoom));
        setLocationMarker(latitude, longitude);
    }

    @Override
    public void moveCamera(double latitude, double longitude, float zoom, float bearing, float tilt) {
        if (map != null) {
            if (!Settings.isTrackFacing()) bearing = 0;
            CameraPosition cameraPosition = CameraPosition.builder(map.getCameraPosition())
                    .target(new LatLng(latitude, longitude))
                    .zoom(zoom)
                    .bearing(bearing)
                    .build();
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void setType(int type) {
        if (type != mapState.mapType) {
            mapState.mapType = type;
            loadMapType(type);
        }
    }

    private void loadMapType(int type) {
        if (map != null) {
            int type1 = GoogleMap.MAP_TYPE_NORMAL;
            switch (type) {
                case MapState.TYPE_SATELLITE:
                    type1 = GoogleMap.MAP_TYPE_SATELLITE;
                    break;
                case MapState.TYPE_HYBRID:
                    type1 = GoogleMap.MAP_TYPE_HYBRID;
                    break;
            }
            map.setMapType(type1);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (!TrackingService.isServiceRunning && marker != null)
            marker.remove();
        deselectPath1();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        CameraPosition cameraPosition = CameraPosition.builder(map.getCameraPosition())
                .target(latLng)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        setMarker(latLng.latitude, latLng.longitude, "Marked location");
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        if (!TrackingService.isServiceRunning) {
            selectPath(polyline);
            mapState.selPathId = getPathID(polyline);
        }
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MarkerItem> cluster) {
        List<String> imagePaths = new ArrayList<>();
        for (MarkerItem item : cluster.getItems()) {
            imagePaths.add(item.getImagePath());
        }
        ClusterImagesFragment fragment = ClusterImagesFragment.newInstance(imagePaths);
        fragment.show(getChildFragmentManager(), fragment.getTag());
    }

    @Override
    public void onClusterItemInfoWindowClick(MarkerItem markerItem) {
        String imageName = markerItem.getImageName();
        if (imageName != null) {
            Intent intent = new Intent(activity, GalleryActivity.class);
            intent.putExtra("imageName", imageName);
            activity.startActivity(intent);
        }
    }

}
