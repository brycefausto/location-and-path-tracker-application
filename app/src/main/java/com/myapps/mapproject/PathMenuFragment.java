package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class PathMenuFragment extends BottomSheetDialogFragment implements OnClickListener {

    private PathMenuListener mListener;
    private EditText editPathName;

    public static PathMenuFragment newInstance(String pathName) {
        Bundle bundle = new Bundle();
        bundle.putString("pathName", pathName);
        PathMenuFragment fragment = new PathMenuFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.fragment_path_menu, null);
        dialog.setContentView(view);
        editPathName = view.findViewById(R.id.editPathName);
        TextView btnSavePath = view.findViewById(R.id.btnSavePath);
        TextView btnDeletePath = view.findViewById(R.id.btnDeletePath);
        String pathName = getArguments().getString("pathName");
        editPathName.setText(pathName);
        btnSavePath.setOnClickListener(this);
        btnDeletePath.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            switch (view.getId()) {
                case R.id.btnSavePath:
                    String pathName1 = editPathName.getText().toString();
                    if (!pathName1.equals("")) {
                        mListener.onSavePathClicked(pathName1);
                        dismiss();
                    } else {
                        Utils.toast(getContext(), "Enter a path name");
                    }
                    break;
                case R.id.btnDeletePath:
                    mListener.onDeletePathClicked();
                    dismiss();
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null)
            mListener.onDeletePathClicked();
    }

    public void setListener(PathMenuListener listener) {
        mListener = listener;
    }

    public interface PathMenuListener {
        void onSavePathClicked(String pathName);

        void onDeletePathClicked();
    }
}
