package com.myapps.mapproject;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.DateTime;


/**
 * A simple {@link Fragment} subclass.
 */
public class PathInfoFragment extends Fragment {

    private Activity activity;
    private PathDao pathDao;
    private UserDao userDao;
    private TextView txtPathID, txtPathName, txtPathUserName, txtPathDateTime, txtPathPoints;
    private PathInfoListener listener;
    private long pathID;

    public PathInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        DaoSession daoSession = ((MapApp) activity.getApplication()).getDaoSession();
        pathDao = daoSession.getPathDao();
        userDao = daoSession.getUserDao();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_path_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtPathID = view.findViewById(R.id.txtPathID);
        txtPathName = view.findViewById(R.id.txtPathName);
        txtPathUserName = view.findViewById(R.id.txtPathUserName);
        txtPathDateTime = view.findViewById(R.id.txtPathDateTime);
        txtPathPoints = view.findViewById(R.id.txtPathPoints);
        view.findViewById(R.id.btnRenamePath).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                renamePath();
            }
        });
        view.findViewById(R.id.btnDeletePath).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePath();
            }
        });
    }

    public void viewPathInfo(long pathID) {
        Path path = PathsData.getPath(pathID);
        if (path != null) {
            this.pathID = pathID;
            User user1 = User.findById(path.getUserId(), userDao);
            String pathIDText = getString(R.string.path_id) + pathID;
            String pathNameText = getString(R.string.path_name) + path.getPathName();
            String userText = getString(R.string.user_name) + user1.getFullName();
            DateTime dt = DateTime.parse(path.getDateTime());
            String dateTimeText = getString(R.string.date_time) + dt.toString("MMM d, yyyy h:mm:ss a");
            String pathPointsText = getString(R.string.path_points) + path.getPointsLatLong().size();
            txtPathID.setText(pathIDText);
            txtPathName.setText(pathNameText);
            txtPathUserName.setText(userText);
            txtPathDateTime.setText(dateTimeText);
            txtPathPoints.setText(pathPointsText);
        }
    }

    public void resetPathInfo() {
        pathID = 0;
        txtPathID.setText(R.string.path_id);
        txtPathName.setText(R.string.path_name);
        txtPathUserName.setText(R.string.user_name);
        txtPathDateTime.setText(R.string.date_time);
        txtPathPoints.setText(R.string.path_points);
    }

    public void setListener(PathInfoListener listener) {
        this.listener = listener;
    }

    public void renamePath() {
        final Path path = PathsData.getPath(pathID);
        if (path != null) {
            Utils.showNameDialog(activity, "Rename path", path.getPathName(), new Utils.NameDialogCallback() {
                @Override
                public void onPositiveClick(AlertDialog dialog, String name) {
                    PathsData.renamePath(pathDao, pathID, name);
                    String nameText = getString(R.string.path_name) + name;
                    txtPathName.setText(nameText);
                    dialog.dismiss();
                }

                @Override
                public void onNegativeClick(AlertDialog dialog) {
                }
            });
        }
    }

    public void deletePath() {
        Path path = PathsData.getPath(pathID);
        if (path != null) {
            Utils.showDialog(activity, "Delete Path", "Do you want to delete this path?", new Utils.DialogCallback() {
                @Override
                public void onPositiveClick(AlertDialog dialog) {
                    PathsData.deletePath(pathDao, pathID);
                    Utils.toast(activity, "Path deleted");
                    if (listener != null) listener.onDeletePath();
                    dialog.dismiss();
                }

                @Override
                public void onNegativeClick(AlertDialog dialog) {
                }
            });
        }
    }

    public interface PathInfoListener {
        void onDeletePath();
    }
}
