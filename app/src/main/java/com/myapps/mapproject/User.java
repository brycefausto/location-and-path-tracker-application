package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class User {

    private static final String KEY_ID = "id";

    @Id(autoincrement = true)
    private Long id;

    @Property
    private String username;

    @Property
    private String password;

    @Property
    private String email;

    @Property
    private String fullName;

    @Generated(hash = 1589870025)
    public User(Long id, String username, String password, String email,
                String fullName) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.fullName = fullName;
    }

    public User(String username, String password, String email, String fullName) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.fullName = fullName;
    }

    public User(String email, String password, String fullName) {
        this.username = "";
        this.password = password;
        this.email = email;
        this.fullName = fullName;
    }

    public User(String email, String fullName) {
        this.username = "";
        this.password = "";
        this.email = email;
        this.fullName = fullName;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public static User newLoginInfo(String username, String password) {
        return new User(username, password, "", "");
    }

    public static User findById(long userID, UserDao userDao) {
        return userDao.queryBuilder()
                .where(UserDao.Properties.Id.eq(userID)).unique();
    }

    @SuppressLint("ApplySharedPref")
    static void saveSignIn(long id1, SharedPreferences pref) {
        pref.edit().putLong(KEY_ID, id1).commit();
    }

    static long getUserId(SharedPreferences pref) {
        return pref.getLong(KEY_ID, 0);
    }

    static User loadUser(SharedPreferences pref, UserDao userDao) {
        long id1 = pref.getLong(KEY_ID, 0);
        return userDao.queryBuilder()
                .where(UserDao.Properties.Id.eq(id1)).unique();
    }

    static void logOut(SharedPreferences pref) {
        pref.edit().remove(KEY_ID).apply();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
