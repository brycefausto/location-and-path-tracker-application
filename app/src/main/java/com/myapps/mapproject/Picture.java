package com.myapps.mapproject;

import android.graphics.Bitmap;

public class Picture {

    boolean hasLatLong;
    private String path;
    private String name;
    private double latitude, longitude;
    private Bitmap image;

    Picture(String path, String name) {
        this.path = path;
        this.name = name;
    }

    void setLatLong(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        hasLatLong = true;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double[] getLatLong() {
        return new double[]{latitude, longitude};
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
