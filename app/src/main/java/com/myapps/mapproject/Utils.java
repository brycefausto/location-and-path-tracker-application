package com.myapps.mapproject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.SphericalUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

@SuppressWarnings("ResultOfMethodCallIgnored")
class Utils {

    public static final int REQUEST_CHECK_SETTINGS = 2;

    private static LocationResultCallback locationResultCallback;

    static void toast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    static void toastOnUI(final Context context, final String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                toast(context, text);
            }
        });
    }

    static void log(String text) {
        Log.d("msg1", text);
    }

    static double hypot(double x, double y) {
        return sqrt(x * x + y * y);
    }

    static float hypot(float x, float y) {
        return (float) sqrt(x * x + y * y);
    }

    static float hypot3(float x, float y, float z) {
        return (float) sqrt(x * x + y * y + z * z);
    }

    static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    static boolean checkPermissions(Context context, String[] permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @SuppressLint("MissingPermission")
    static void requestLocationUpdates(final FusedLocationProviderClient locationProviderClient, long timeInterval, final boolean isOnce, final LocationResultCallback callback) {
        final LocationRequest locationRequest = new LocationRequest()
                .setInterval(timeInterval + 2000)
                .setFastestInterval(timeInterval)
                .setSmallestDisplacement(1)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        final LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                if (location != null) {
                    if (isOnce)
                        locationProviderClient.removeLocationUpdates(this);
                    callback.onLocationResult(location);
                }
            }
        };
        removeLocationUpdates();
        callback.locationProviderClient = locationProviderClient;
        callback.locationCallback = locationCallback;
        locationResultCallback = callback;
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    static void removeLocationUpdates() {
        if (locationResultCallback != null) {
            FusedLocationProviderClient locationProviderClient = locationResultCallback.locationProviderClient;
            LocationCallback locationCallback = locationResultCallback.locationCallback;
            if (locationProviderClient != null && locationCallback != null) {
                locationProviderClient.removeLocationUpdates(locationCallback);
            }
        }
    }

    static void setLocationSettings(final Activity activity, final OnSuccessListener<LocationSettingsResponse> successListener) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(new LocationRequest());
        SettingsClient client = LocationServices.getSettingsClient(activity);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(successListener);
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException ignored) {
                    }
                }
            }
        });
    }

    static void setLocationSettings(final Activity activity) {
        setLocationSettings(activity, null);
    }

    static float getDeltaTime(Location curLocation, Location lastLocation) {
        return (float) (curLocation.getElapsedRealtimeNanos() - lastLocation.getElapsedRealtimeNanos()) / 1000000000;
    }

    static float getSpeed(Location curLocation, Location lastLocation) {
        float dt = getDeltaTime(curLocation, lastLocation);
        float acc = curLocation.getAccuracy();
        float dist = lastLocation.distanceTo(curLocation);
        float speedAcc = acc / dt;
        Bundle bundle;
        if (curLocation.getExtras() != null)
            bundle = curLocation.getExtras();
        else
            bundle = new Bundle();
        bundle.putFloat("speedAcc", speedAcc);
        curLocation.setExtras(bundle);
        return dist / dt;
    }

    static float getSpeedAccuracy(Location location) {
        Bundle bundle = location.getExtras();
        if (bundle != null) {
            return bundle.getFloat("speedAcc", 0);
        }
        return 0;
    }

    static float[] getXYFromLocation(float vector, float angle) {
        float x = vector * (float) Math.sin(angle);
        float y = vector * (float) Math.cos(angle);
        return new float[] {x, y};
    }

    private static float[] getRotationVector(float[] orientation) {
        double c1 = Math.cos(orientation[0] / 2);
        double s1 = Math.sin(orientation[0] / 2);

        double c2 = Math.cos(-orientation[1] / 2);
        double s2 = Math.sin(-orientation[1] / 2);

        double c3 = Math.cos(orientation[2] / 2);
        double s3 = Math.sin(orientation[2] / 2);

        double c1c2 = c1 * c2;
        double s1s2 = s1 * s2;

        double w = c1c2 * c3 - s1s2 * s3;
        double x = c1c2 * s3 + s1s2 * c3;
        double y = s1 * c2 * c3 + c1 * s2 * s3;
        double z = c1 * s2 * c3 - s1 * c2 * s3;
        return new float[] {(float) w, (float) z, (float) x, (float) y};
    }

    static float[] getGyroscopeRotationVector(float[] prevRV, float[] RV) {
        final double q1a = prevRV[0];
        final double q1b = prevRV[1];
        final double q1c = prevRV[2];
        final double q1d = prevRV[3];

        final double q2a = RV[0];
        final double q2b = RV[1];
        final double q2c = RV[2];
        final double q2d = RV[3];
        final double w = q1a * q2a - q1b * q2b - q1c * q2c - q1d * q2d;
        final double x = q1a * q2b + q1b * q2a + q1c * q2d - q1d * q2c;
        final double y = q1a * q2c - q1b * q2d + q1c * q2a + q1d * q2b;
        final double z = q1a * q2d + q1b * q2c - q1c * q2b + q1d * q2a;
        return new float[] {(float) w, (float) x, (float) y, (float) z};
    }

    static void calculateFusedMatrix(float[] RMatrix, float[] RV) {
        float alpha = 0.98f;
        float oneMinusAlpha = 1 - alpha;
        float[] orientation = new float[3];
        SensorManager.getOrientation(RMatrix, orientation);
        float[] rotationVectorAccMag = getRotationVector(orientation);
        float[] rotationVectorGyro = getGyroscopeRotationVector(rotationVectorAccMag, RV);
        for (int i = 0; i < RV.length; i++) {
            rotationVectorGyro[i] = alpha * rotationVectorGyro[i] + oneMinusAlpha * rotationVectorAccMag[i];
        }
        float[] fusedVector = new float[4];
        fusedVector[0] = rotationVectorGyro[1];
        fusedVector[1] = rotationVectorGyro[2];
        fusedVector[2] = rotationVectorGyro[3];
        fusedVector[3] = rotationVectorGyro[0];
        float[] fusedMatrix = new float[16];
        SensorManager.getRotationMatrixFromVector(fusedMatrix, fusedVector);
        System.arraycopy(fusedMatrix, 0, RMatrix, 0, RMatrix.length);
    }

    static float[] getAngles(float[] RMatrix) {
        float[] orientation = new float[3];
        SensorManager.getOrientation(RMatrix, orientation);
        for (int i = 0; i < orientation.length; i++) {
            orientation[i] = (float) Math.toDegrees(orientation[i]);
        }
        return orientation;
    }

    static double[] getHorizontalXY(double east, double north, float bearing) {
        bearing = -bearing;
        double x = north * sin(bearing) + east * sin(bearing + 90);
        double y = north * cos(bearing) + east * cos(bearing + 90);
        return new double[]{x, y};
    }

    static double[] getEastNorth(double x, double y, float bearing) {
        double east = y * sin(bearing) + x * sin(bearing + 90);
        double north = y * cos(bearing) + x * cos(bearing + 90);
        return new double[]{east, north};
    }

    static double[] computeOffset(double latitude, double longitude, double dNorth, double dEast) {
        LatLng latLng = new LatLng(latitude, longitude);
        latLng = SphericalUtil.computeOffset(latLng, dNorth, 0);
        latLng = SphericalUtil.computeOffset(latLng, dEast, 90);
        return new double[]{latLng.latitude, latLng.longitude};
    }

    static float getTrueBearing(float bearing, double latitude, double longitude) {
        GeomagneticField gmf = new GeomagneticField((float) latitude, (float) longitude, 1, System.currentTimeMillis());
        return bearing + gmf.getDeclination();
    }

    static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    static boolean isValidUsername(Context context, String username) {
        if (username.equals("")) {
            Utils.toast(context, "Please enter your username");
            return false;
        }
        if (!username.matches("^[a-zA-Z](?:[_-]?[a-zA-Z0-9]){2,25}")) {
            Utils.toast(context, "The username is invalid");
            return false;
        }
        return true;
    }

    static boolean isValidUsername1(Context context, String username) {
        if (username.matches("^[a-zA-Z](?:[_-]?[a-zA-Z0-9]){2,25}") || username.equals(""))
            return true;
        Utils.toast(context, "The username is invalid");
        return false;
    }

    static boolean isValidEmail(Context context, String email) {
        if (email.equals("")) {
            Utils.toast(context, "Please enter your email");
            return false;
        }
        if (!email.matches(Patterns.EMAIL_ADDRESS.pattern())) {
            Utils.toast(context, "The email is invalid");
            return false;
        }
        return true;
    }

    static boolean isValidUsernameEmail(Context context, String username) {
        if (username.equals("")) {
            Utils.toast(context, "Please enter your username/email");
            return false;
        }
        if (!(username.matches("^[a-zA-Z](?:[_-]?[a-zA-Z0-9]){2,25}") ||
                username.matches(Patterns.EMAIL_ADDRESS.pattern()))) {
            Utils.toast(context, "The username/email is invalid");
            return false;
        }
        return true;
    }

    static boolean isValidFullName(Context context, String fullName) {
        if (fullName.equals("")) {
            Utils.toast(context, "Please enter your full name");
            return false;
        }
        if (!fullName.matches("^[a-zA-Z0-9 ,.\\'-]{2,45}")) {
            Utils.toast(context, "The full name is invalid");
            return false;
        }
        return true;
    }

    static boolean isValidPassword(Context context, String password) {
        if (password.equals("")) {
            Utils.toast(context, "Please enter your password");
            return false;
        }
        if (password.length() < 4) {
            Utils.toast(context, "The password is invalid");
            return false;
        }
        return true;
    }

    static boolean isValidPasswords(Context context, String password, String retypePassword) {
        isValidPassword(context, password);
        if (retypePassword.equals("")) {
            Utils.toast(context, "Please retype your password");
            return false;
        }
        if (!password.equals(retypePassword)) {
            Utils.toast(context, "The passwords are not matched");
            return false;
        }
        return true;
    }

    static void showDialog(AlertDialog.Builder builder, String title, String message, final DialogCallback callback) {
        final AlertDialog dialog = builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Done", null)
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
        final Button btnPositive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        final Button btnNegative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == btnPositive)
                    callback.onPositiveClick(dialog);
                else if (view == btnNegative) {
                    callback.onNegativeClick(dialog);
                    dialog.dismiss();
                }
            }
        };
        btnPositive.setOnClickListener(listener);
        btnNegative.setOnClickListener(listener);
    }

    static void showDialog(Context context, String title, String message, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        showDialog(builder, title, message, callback);
    }

    static void showAlertDialog(Context context, String title, String message, final DialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog dialog = builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onPositiveClick(dialog);
                dialog.dismiss();
            }
        });
    }


    static void showNameDialog(final Context context, String title, String defaultName, final NameDialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.name_dialog, null);
        builder.setView(view);
        final EditText editName = view.findViewById(R.id.editName);
        editName.setText(defaultName);
        final AlertDialog dialog = builder.setTitle(title)
                .setPositiveButton("Done", null)
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
        final Button btnPositive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        final Button btnNegative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == btnPositive) {
                    String name = editName.getText().toString();
                    if (!name.equals(""))
                        callback.onPositiveClick(dialog, editName.getText().toString());
                    else
                        toast(context, "Enter a name");
                } else if (view == btnNegative) {
                    callback.onNegativeClick(dialog);
                    dialog.dismiss();
                }
            }
        };
        btnPositive.setOnClickListener(listener);
        btnNegative.setOnClickListener(listener);
    }

    public static void showInternetAlert(Context context) {
        Utils.showAlertDialog(context, "The device has no internet", "Please connect to Wifi or mobile data", new Utils.DialogCallback() {
            @Override
            public void onPositiveClick(AlertDialog dialog) {
            }

            @Override
            public void onNegativeClick(AlertDialog dialog) {
            }
        });
    }

    public static boolean checkInternetWithAlert(Context context) {
        boolean hasInternet = isNetworkAvailable(context);
        if (!hasInternet)
            showInternetAlert(context);
        return hasInternet;
    }

    static File getPicturesDirectory() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Map Project");
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdir()) {
            return null;
        }
        return mediaStorageDir;
    }

    static File newPictureFile(String fileName) {
        return new File(getPicturesDirectory().getPath() + File.separator + fileName + ".jpg");
    }

    static void writeToFile(String filePath, String text) {
        File file = new File(filePath);
        if (file.exists()) file.delete();
        try (FileOutputStream os = new FileOutputStream(filePath)) {
            os.write(text.getBytes());
            os.close();
        } catch (IOException ignored) {
        }
    }

    static String readFile(File file) {
        try (FileInputStream is = new FileInputStream(file)) {
            byte[] bytes = new byte[(int) file.length()];
            is.read(bytes);
            is.close();
            return new String(bytes);
        } catch (IOException e) {
            return null;
        }
    }

    static String removeFileExtension(String fileName) {
        return fileName.replaceFirst("[.][^.]+$", "");
    }

    public interface DialogCallback {
        void onPositiveClick(AlertDialog dialog);

        void onNegativeClick(AlertDialog dialog);
    }

    public interface NameDialogCallback {
        void onPositiveClick(AlertDialog dialog, String name);

        void onNegativeClick(AlertDialog dialog);
    }

    public static abstract class LocationResultCallback {

        FusedLocationProviderClient locationProviderClient;
        LocationCallback locationCallback;

        abstract void onLocationResult(Location location);
    }

}