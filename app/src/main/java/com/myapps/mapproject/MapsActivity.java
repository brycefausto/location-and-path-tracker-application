package com.myapps.mapproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private static int selMap;
    private OfflineManager offlineManager;
    private MapAdapter adapter;
    private LongSparseArray<OfflineRegion> offlineRegionArray = new LongSparseArray<>();

    public static byte[] putMapInfo(byte[] metadata, String mapDate, long mapSize) {
        try {
            String json = new String(metadata, IMap.JSON_CHARSET);
            JSONObject jsonObject = new JSONObject(json);
            jsonObject.put(IMap.JSON_FIELD_MAP_DATE, mapDate);
            jsonObject.put(IMap.JSON_FIELD_MAP_SIZE, mapSize);
            return jsonObject.toString().getBytes(IMap.JSON_CHARSET);
        } catch (Exception exception) {
            return null;
        }
    }

    private static OfflineMap offlineRegionToMap(OfflineRegion offlineRegion) {
        byte[] metadata = offlineRegion.getMetadata();
        long regionId = offlineRegion.getID();
        try {
            String json = new String(metadata, IMap.JSON_CHARSET);
            JSONObject jsonObject = new JSONObject(json);
            String mapName = jsonObject.getString(IMap.JSON_FIELD_REGION_NAME);
            String mapDate = jsonObject.getString(IMap.JSON_FIELD_MAP_DATE);
            long mapSize = jsonObject.getLong(IMap.JSON_FIELD_MAP_SIZE);
            return new OfflineMap(regionId, mapName, mapDate, mapSize);
        } catch (Exception exception) {
            return null;
        }
    }

    public static byte[] renameMap(byte[] metadata, String mapName) {
        try {
            String json = new String(metadata, IMap.JSON_CHARSET);
            JSONObject jsonObject = new JSONObject(json);
            jsonObject.put(IMap.JSON_FIELD_REGION_NAME, mapName);
            return jsonObject.toString().getBytes(IMap.JSON_CHARSET);
        } catch (Exception exception) {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ListView listView = findViewById(R.id.listView);
        adapter = new MapAdapter(this);
        listView.setAdapter(adapter);
        offlineManager = OfflineManager.getInstance(this);
        loadOfflineMaps();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    //Load a list of offline maps
    private void loadOfflineMaps() {
        offlineManager.listOfflineRegions(new OfflineManager.ListOfflineRegionsCallback() {
            @Override
            public void onList(OfflineRegion[] offlineRegions) {
                for (OfflineRegion offlineRegion : offlineRegions) {
                    offlineRegionArray.put(offlineRegion.getID(), offlineRegion);
                    OfflineMap map = offlineRegionToMap(offlineRegion);
                    if (map != null)
                        adapter.add(map);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String error) {
            }
        });
    }

    public void addMap(View v) {
        finish();
        startActivity(new Intent(this, MapSelectorActivity.class));
    }

    //For popup menu
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        final OfflineMap map = adapter.getItem(selMap);
        final OfflineRegion offlineRegion = offlineRegionArray.get(map.regionId);
        switch (menuItem.getItemId()) {
            case R.id.view:
                CameraPosition cameraPosition =
                        OfflineUtils.getCameraPosition((OfflineTilePyramidRegionDefinition) offlineRegion.getDefinition());
                MapState mapState = MapState.getInstance();
                LatLng target = cameraPosition.target;
                mapState.cameraTarget = new Point(target.getLatitude(), target.getLongitude());
                mapState.setCameraZoom1(cameraPosition.zoom);
                mapState.cameraBearing = 0;
                mapState.cameraTilt = 0;
                mapState.isMapBox = true;
                finish();
                startActivity(new Intent(this, MainActivity.class));
                return true;
            case R.id.rename:
                Utils.showNameDialog(this, "Rename Map", map.mapName, new Utils.NameDialogCallback() {
                    @Override
                    public void onPositiveClick(AlertDialog dialog, String name) {
                        byte[] metadata = renameMap(offlineRegion.getMetadata(), name);
                        map.mapName = name;
                        adapter.notifyDataSetChanged();
                        offlineRegion.updateMetadata(metadata, new OfflineRegion.OfflineRegionUpdateMetadataCallback() {
                            @Override
                            public void onUpdate(byte[] metadata) {
                            }

                            @Override
                            public void onError(String error) {
                            }
                        });
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeClick(AlertDialog dialog) {
                    }
                });
                return true;
            case R.id.delete:
                Utils.showDialog(this, "Delete map", "Do you want to delete this map?", new Utils.DialogCallback() {
                    @Override
                    public void onPositiveClick(AlertDialog dialog) {
                        adapter.remove(map);
                        adapter.notifyDataSetChanged();
                        offlineRegion.delete(new OfflineRegion.OfflineRegionDeleteCallback() {
                            @Override
                            public void onDelete() {
                            }

                            @Override
                            public void onError(String error) {
                            }
                        });
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeClick(AlertDialog dialog) {
                    }
                });
                return true;
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventDownload(MapDownloadService.DownloadEvent event) {
        if (event.type == MapDownloadService.EVENT_COMPLETE) {
            //When a map is downloaded, update offline map list
            adapter.clear();
            loadOfflineMaps();
        }
    }

    private static class OfflineMap {

        long regionId;
        String mapName, mapDate;
        long mapSize;

        OfflineMap(long regionId, String mapName, String mapDate, long mapSize) {
            this.regionId = regionId;
            this.mapName = mapName;
            this.mapDate = mapDate;
            this.mapSize = mapSize;
        }
    }

    private static class MapAdapter extends BaseAdapter {

        private Context context;
        private List<OfflineMap> list = new ArrayList<>();
        private LayoutInflater inflater;

        MapAdapter(Context context) {
            this.context = context;
            inflater = LayoutInflater.from(context);
        }

        public void add(OfflineMap map) {
            list.add(map);
        }

        public void remove(OfflineMap map) {
            list.remove(map);
        }

        public void clear() {
            list.clear();
        }

        private void showPopupMenu(View v) {
            PopupMenu popupMenu = new PopupMenu(context, v);
            popupMenu.inflate(R.menu.map_menu);
            popupMenu.setOnMenuItemClickListener((MapsActivity) context);
            popupMenu.show();
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public OfflineMap getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            ViewHolder vh;
            if (view == null) {
                view = inflater.inflate(R.layout.offline_map_item, viewGroup, false);
                vh = new ViewHolder(view);
                view.setTag(vh);
            } else {
                vh = (ViewHolder) view.getTag();
            }
            OfflineMap map = getItem(i);
            String mapSizeText = (map.mapSize / 1048576) + " MB";
            DateTime dt = DateTime.parse(map.mapDate);
            String dateText = dt.toString("MMM d, yyyy h:mm:ss a");
            String mapInfoText = mapSizeText + " " + dateText;
            vh.txtMapName.setText(map.mapName);
            vh.txtMapInfo.setText(mapInfoText);
            vh.btnMapMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selMap = i;
                    showPopupMenu(view);
                }
            });
            return view;
        }

        private static class ViewHolder {
            TextView txtMapName, txtMapInfo;
            ImageButton btnMapMenu;

            ViewHolder(View v) {
                txtMapName = v.findViewById(R.id.txtMapName);
                txtMapInfo = v.findViewById(R.id.txtMapInfo);
                btnMapMenu = v.findViewById(R.id.btnMapMenu);
            }
        }
    }
}
