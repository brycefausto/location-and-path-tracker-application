package com.myapps.mapproject;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineRegionError;
import com.mapbox.mapboxsdk.offline.OfflineRegionStatus;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class MapDownloadService extends Service {

    public static final String ACTION_CANCEL = "CANCEL",
            KEY_METADATA = "metadata",
            KEY_DEFINITION = "definition";
    public static final int EVENT_PROGRESS = 0, EVENT_COMPLETE = 1, EVENT_CANCELED = 2, EVENT_ERROR = 3;

    public static boolean isRunning;

    private static int notificationId = 1021;

    private final Context context = this;

    private OfflineManager offlineManager;
    private NotificationManager nManager;
    private OfflineRegion mOfflineRegion;
    private boolean isDownloading;
    private List<MapDownload> mapDownloads = new ArrayList<>();

    public MapDownloadService() {
    }

    public static void startDownload(Context context, OfflineTilePyramidRegionDefinition definition, byte[] metadata) {
        Intent intent = new Intent(context, MapDownloadService.class);
        intent.putExtra(KEY_DEFINITION, definition);
        intent.putExtra(KEY_METADATA, metadata);
        context.startService(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
        offlineManager = OfflineManager.getInstance(this);
        nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) {
            OfflineTilePyramidRegionDefinition definition = intent.getParcelableExtra(KEY_DEFINITION);
            byte[] metadata = intent.getByteArrayExtra(KEY_METADATA);
            if (definition != null && metadata != null) {
                if (isDownloading)
                    mapDownloads.add(new MapDownload(definition, metadata));
                else
                    downloadMap(definition, metadata);
            }
        } else if (action.equals(ACTION_CANCEL)) {
            cancelDownload();
        }
        return START_STICKY;
    }

    private void downloadMap(OfflineTilePyramidRegionDefinition definition, byte[] metadata) {
        isDownloading = true;
        final NotificationCompat.Builder nBuilder = createNotificationBuilder();
        nBuilder.setProgress(100, 0, false);
        nManager.notify(notificationId, nBuilder.build());
        offlineManager.createOfflineRegion(definition, metadata, new OfflineManager.CreateOfflineRegionCallback() {
            @Override
            public void onCreate(final OfflineRegion offlineRegion) {
                mOfflineRegion = offlineRegion;
                offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);
                offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver() {
                    @Override
                    public void onStatusChanged(OfflineRegionStatus status) {
                        double percentage = status.getRequiredResourceCount() >= 0
                                ? (100.0 * status.getCompletedResourceCount() / status.getRequiredResourceCount()) :
                                0.0;
                        DownloadEvent event = new DownloadEvent();
                        if (status.isComplete()) {
                            event.type = EVENT_COMPLETE;
                            event.offlineRegion = offlineRegion;
                            MapDownloadService.this.completeDownload(offlineRegion, status);
                            nBuilder.setContentText("Download complete")
                                    .setProgress(0, 0, false);
                        } else {
                            int progress = (int) percentage;
                            event.type = EVENT_PROGRESS;
                            event.progress = progress;
                            nBuilder.setProgress(100, progress, false);
                        }
                        nManager.notify(notificationId, nBuilder.build());
                        EventBus.getDefault().post(event);
                    }

                    @Override
                    public void onError(OfflineRegionError error) {
                        errorDownload();
                    }

                    @Override
                    public void mapboxTileCountLimitExceeded(long limit) {
                        errorDownload();
                    }
                });
            }

            @Override
            public void onError(String error) {
                errorDownload();
            }
        });
    }

    private NotificationCompat.Builder createNotificationBuilder() {
        Intent cancelIntent = new Intent(this, this.getClass());
        cancelIntent.setAction(ACTION_CANCEL);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0,
                cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Action action = new NotificationCompat.Action(
                android.R.drawable.ic_menu_close_clear_cancel,
                "Cancel",
                pendingIntent
        );
        PendingIntent pendingIntent1 = PendingIntent.getActivity(this, 0,
                new Intent(this, MapSelectorActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(this, "1")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentTitle("Map Download")
                .setContentText("Downloading map")
                .setContentIntent(pendingIntent1)
                .addAction(action);
    }

    public void completeDownload(OfflineRegion offlineRegion, OfflineRegionStatus status) {
        isDownloading = false;
        String dateTime = new DateTime().toString();
        long mapSize = status.getCompletedResourceSize();
        byte[] metadata = MapsActivity.putMapInfo(offlineRegion.getMetadata(), dateTime, mapSize);
        offlineRegion.updateMetadata(metadata, new OfflineRegion.OfflineRegionUpdateMetadataCallback() {
            @Override
            public void onUpdate(byte[] metadata) {
                String regionName = OfflineUtils.convertRegionName(metadata);
                Utils.toast(context, regionName + " is downloaded");
                nextDownload();
            }

            @Override
            public void onError(String error) {
                stopSelf();
            }
        });
    }

    private void errorDownload() {
        isDownloading = false;
        DownloadEvent event = new DownloadEvent();
        event.type = EVENT_ERROR;
        EventBus.getDefault().post(event);
        stopSelf();
    }

    private void cancelDownload() {
        isDownloading = false;
        if (mOfflineRegion != null) {
            mOfflineRegion.setDownloadState(OfflineRegion.STATE_INACTIVE);
            mOfflineRegion.setObserver(null);
            mOfflineRegion.delete(new OfflineRegion.OfflineRegionDeleteCallback() {
                @Override
                public void onDelete() {
                    DownloadEvent event = new DownloadEvent();
                    event.type = EVENT_CANCELED;
                    EventBus.getDefault().post(event);
                }

                @Override
                public void onError(String error) {
                    errorDownload();
                }
            });
        }
        stopSelf();
    }

    private void nextDownload() {
        if (mapDownloads.size() > 0) {
            MapDownload mapDownload = mapDownloads.get(0);
            downloadMap(mapDownload.definition, mapDownload.metadata);
            mapDownloads.remove(mapDownload);
        } else {
            stopSelf();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        nManager.cancel(notificationId);
    }

    public static class DownloadEvent {
        int type, progress;
        OfflineRegion offlineRegion;
    }

    private static class MapDownload {
        OfflineTilePyramidRegionDefinition definition;
        byte[] metadata;

        public MapDownload(OfflineTilePyramidRegionDefinition definition, byte[] metadata) {
            this.definition = definition;
            this.metadata = metadata;
        }
    }

}
