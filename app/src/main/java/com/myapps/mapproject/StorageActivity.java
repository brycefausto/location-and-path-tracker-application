package com.myapps.mapproject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class StorageActivity extends AppCompatActivity {

    private static final String KEY_HAS_UPLOADED_FILES = "hasUploadedFiles";

    private final Activity activity = this;

    private View downloadOption, deleteOption;
    private TextView txtDownload;
    private ImageButton btnDownload;
    private SharedPreferences pref;
    private boolean hasUploadedFiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);
        downloadOption = findViewById(R.id.downloadOption);
        deleteOption = findViewById(R.id.deleteOption);
        txtDownload = findViewById(R.id.txtDownload);
        btnDownload = findViewById(R.id.btnDownload);
        pref = getSharedPreferences("pref", MODE_PRIVATE);
        final long userId = User.getUserId(pref);
        hasUploadedFiles = pref.getBoolean(KEY_HAS_UPLOADED_FILES, false);
        startService(new Intent(this, AppDataService.class));
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("userId", String.valueOf(userId))
                .add("fileNames[]", "user_paths.dat")
                .add("fileNames[]", "offline_maps.dat")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/check_files.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                downloadOption.setVisibility(View.VISIBLE);
                if (!hasUploadedFiles)
                    disableDownload();
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                downloadOption.setVisibility(View.VISIBLE);
                if (response.isSuccessful() && responseStr.equals("exists")) {
                    pref.edit().putBoolean(KEY_HAS_UPLOADED_FILES, true).apply();
                    deleteOption.setVisibility(View.VISIBLE);
                } else {
                    disableDownload();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void disableDownload() {
        int lightGray = ContextCompat.getColor(activity, R.color.lightGray);
        txtDownload.setText(R.string.download_text1);
        txtDownload.setTextColor(lightGray);
        btnDownload.setEnabled(false);
        btnDownload.setColorFilter(lightGray);
        deleteOption.setVisibility(View.GONE);
        pref.edit().putBoolean(KEY_HAS_UPLOADED_FILES, false).apply();
    }

    private void enableDownload() {
        txtDownload.setText(R.string.download_text);
        txtDownload.setTextColor(ContextCompat.getColor(activity, android.R.color.primary_text_light));
        btnDownload.setEnabled(true);
        btnDownload.setColorFilter(0);
        deleteOption.setVisibility(View.VISIBLE);
        pref.edit().putBoolean(KEY_HAS_UPLOADED_FILES, true).apply();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFileEvent(String event) {
        Log.d("msg1", "file event");
        switch (event) {
            case "File_uploaded":
                Log.d("msg1", "file uploaded");
                enableDownload();
                break;
            case "File_deleted":
                disableDownload();
        }
    }

    public void onUploadClicked(View v) {
        Intent intent = new Intent(this, FileService.class);
        startService(intent);
    }

    public void onDownloadClicked(View v) {
        Intent intent = new Intent(this, FileService.class);
        intent.setAction(FileService.ACTION_DOWNLOAD);
        startService(intent);
    }

    public void onDeleteClicked(View v) {
        Intent intent = new Intent(this, FileService.class);
        intent.setAction(FileService.ACTION_DELETE);
        startService(intent);
    }

}
