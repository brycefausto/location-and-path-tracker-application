package com.myapps.mapproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.Cluster;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.ClusterManagerPlugin;
import com.mapbox.mapboxsdk.plugins.cluster.clustering.view.DefaultClusterRenderer;
import com.mapbox.mapboxsdk.plugins.cluster.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;

public class MyItemRenderer extends DefaultClusterRenderer<MyItem> {

    private Context context;
    private IconGenerator clusterIconGenerator, itemIconGenerator;
    private TextView txtImageName;
    private ImageView imageView, imgMarkerImage;
    private Icon defaultClusterIcon, defaultItemIcon;

    public MyItemRenderer(Context context, MapboxMap map, ClusterManagerPlugin<MyItem> clusterManagerPlugin) {
        super(context, map, clusterManagerPlugin);
        this.context = context;
        clusterIconGenerator = new IconGenerator(context);
        itemIconGenerator = new IconGenerator(context);
        View view = LayoutInflater.from(context).inflate(R.layout.image_cluster, null);
        imageView = view.findViewById(R.id.imageView);
        clusterIconGenerator.setContentView(view);
        View view1 = LayoutInflater.from(context).inflate(R.layout.custom_marker, null);
        txtImageName = view1.findViewById(R.id.txtImageName);
        imgMarkerImage = view1.findViewById(R.id.imgMarkerImage);
        txtImageName.setText("");
        itemIconGenerator.setContentView(view1);
        IconFactory iconFactory = IconFactory.getInstance(context);
        Bitmap bitmap = clusterIconGenerator.makeIcon();
        Bitmap bitmap1 = itemIconGenerator.makeIcon();
        defaultClusterIcon = iconFactory.fromBitmap(bitmap);
        defaultItemIcon = iconFactory.fromBitmap(bitmap1);
    }

    @Override
    protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {
        if (item.getImagePath() != null) {
            markerOptions.icon(defaultItemIcon);
        }
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MyItem> cluster, MarkerOptions markerOptions) {
        markerOptions.icon(defaultClusterIcon).setTitle("View images");
    }

    @Override
    protected void onClusterItemRendered(final MyItem clusterItem, final Marker marker) {
        GlideApp.with(context)
                .load(clusterItem.getImagePath())
                .override(120)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        if (getClusterItem(marker) != null) {
                            txtImageName.setText(clusterItem.getImageName());
                            imgMarkerImage.setImageDrawable(resource);
                            Bitmap icon = itemIconGenerator.makeIcon();
                            marker.setIcon(IconFactory.recreate(clusterItem.getImageName(), icon));
                        }
                    }
                });
    }

    @Override
    protected void onClusterRendered(final Cluster<MyItem> cluster, final Marker marker) {
        final List<Drawable> imageDrawables = new ArrayList<>(Math.min(4, cluster.getSize()));
        final int width = imageView.getWidth();
        final int height = imageView.getHeight();
        final int[] counter = {0};
        final String[] clusterName = {""};
        for (MyItem item : cluster.getItems()) {
            if (counter[0] == 4) break;
            counter[0]++;
            clusterName[0] += item.getImageName();
            GlideApp.with(context)
                    .load(item.getImagePath())
                    .override(100)
                    .centerCrop()
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            resource.setBounds(0, 0, width, height);
                            imageDrawables.add(resource);
                            if (imageDrawables.size() == counter[0] && getCluster(marker) != null) {
                                MultiDrawable multiDrawable = new MultiDrawable(imageDrawables);
                                multiDrawable.setBounds(0, 0, width, height);
                                imageView.setImageDrawable(multiDrawable);
                                Bitmap icon = clusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                                marker.setIcon(IconFactory.recreate(clusterName[0], icon));
                            }
                        }
                    });
        }
    }

    protected void createImageMarker(final Marker marker, final String imageName, String imagePath) {
        GlideApp.with(context)
                .load(imagePath)
                .override(120)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        txtImageName.setText(imageName);
                        imgMarkerImage.setImageDrawable(resource);
                        Bitmap icon = itemIconGenerator.makeIcon();
                        marker.setIcon(IconFactory.recreate(imageName, icon));
                    }
                });
        marker.setIcon(defaultItemIcon);
    }

}
