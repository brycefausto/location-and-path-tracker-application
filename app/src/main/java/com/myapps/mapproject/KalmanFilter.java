package com.myapps.mapproject;

//Kalman Filter with position and velocity model
public class KalmanFilter {

    //Previous state => {position, velocity}
    private static double[] Xk_1 = {0, 0};

    //Previous error covariance matrix => {var1, 0, 0, var2}
    private static double[] Pk_1 = {0, 0, 0, 0};

    //Measurement error covariance matrix => {var1, 0, 0, var2}
    private static double[] R = {0, 0, 0, 0};

    public KalmanFilter() {
    }

    public void setError(double posError, double velError) {
        Pk_1[0] = posError * posError;
        Pk_1[3] = velError * velError;
    }

    /**
     * Estimates the most accurate values
     * @param acc acceleration (control vector)
     * @param dt delta time
     * @param mmPos position of measurement state
     * @param mmVel velocity of measurement state
     * @return
     */
    public double[] estimate(double acc, double dt, double mmPos, double mmVel, double pPosError, double pVelError, double mmPosError, double mmVelError) {
        //All equations are simplified
        //Predict state
        double[] Xk = new double[2];
        double dtSqrO2 = dt * dt * 0.5;
        //Set transition matrix
        /*
            [1, dt]
            [0, 1 ]
         */
        double[] A = new double[]{1, dt, 0, 1};
        //Set control matrix
        /*
            [1/2dt^2]
            [dt      ]
         */
        double[] B = new double[]{dtSqrO2, dt};
        //Set control vector
        double[] u = {acc};
        //Reset position to zero to estimate distance
        Xk_1[0] = 0;
        //Xk = A * Xk-1 + B * u
        //Predict position
        Xk[0] = Xk_1[0] + A[1] * Xk_1[1] + B[0] * u[0];
        //Predict velocity
        Xk[1] = Xk_1[1] + B[1] * u[0];

        //Q = process noise covariance matrix
        double[] Qk = {pPosError * pPosError, 0, 0, pVelError * pVelError};

        //Get error covariance matrix
        /*
            Pk = A * Pk_1 * A^T + Qk
         */
        double[] Pk = {Pk_1[0] + Pk_1[3] * A[1] * A[1] + Qk[0], 0, 0, Pk_1[3] + Qk[3]};

        //Set measurement error
        R[0] = mmPosError * mmPosError;
        R[3] = mmVelError * mmVelError;

        //Get Kalman gain
        /*
            Kk = Pk * H^T / (H * Pk * H^T + R)
            Where H = H^T = identity matrix
         */
        Utils.log("Pk[0]: " + Pk[0]);
        Utils.log("R[0]: " + R[0]);
        double[] Kk = {Pk[0] / (Pk[0] + R[0]), 0, 0, Pk[3] / (Pk[3] + R[3])};

        //Set measurement state
        double[] Zk = {mmPos, mmVel};
        Utils.log("Xk[0]: " + Xk[0]);
        Utils.log("Kk[0]: " + Kk[0]);

        //Update state
        double[] Xk_u = new double[2];
        Xk_u[0] = Xk[0] + Kk[0] * (Zk[0] - Xk[0]);
        Xk_u[1] = Xk[1] + Kk[3] * (Zk[1] - Xk[1]);

        //Update error covariance matrix
        /*
            Pk_u = (I - Kk) * Pk
            Where I = identity matrix
         */
        double[] Pk_u = new double[4];
        Pk_u[0] = (1 - Kk[0]) * Pk[0];
        Pk_u[3] = (1- Kk[3]) * Pk[3];

        //Set updated values to previous values
        Xk_1 = Xk_u;
        Pk_1 = Pk_u;

        return Xk_u;
    }

    public float getAccuracy() {
        return (float) Math.sqrt(Pk_1[0]);
    }

    public float getSpeedAccuracy() {
        return (float) Math.sqrt(Pk_1[3]);
    }

}
