package com.myapps.mapproject;

import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.OnSuccessListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PopupMenu.OnMenuItemClickListener {

    private final Activity activity = this;

    private SharedPreferences pref;
    private FragmentManager fragmentManager;
    private MapFragment mapFragment, mapFragment1;
    private MapFragment.MapFragmentListener mapFragmentListener;
    private MapState mapState;
    private PathDao pathDao;
    private FusedLocationProviderClient locationProviderClient;
    private ToggleButton btnTrack;
    private BottomSheetBehavior bottomSheetBehavior;
    private PathInfoFragment pathInfo;
    private DrawerLayout drawerLayout;
    private User user;
    private MenuItem showHidePaths, showHideImages, changeMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnTrack = findViewById(R.id.btnTrack);
        if (TrackingService.isServiceRunning) {
            btnTrack.setChecked(true);
        }
        btnTrack.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                onTrackClick(b);
            }
        });
        drawerLayout = findViewById(R.id.drawerLayout);
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheet));
        fragmentManager = getSupportFragmentManager();
        pathInfo = (PathInfoFragment) fragmentManager.findFragmentById(R.id.pathInfo);
        pathInfo.setListener(new PathInfoFragment.PathInfoListener() {
            @Override
            public void onDeletePath() {
                mapFragment.deleteSelectedPath();
                pathInfo.resetPathInfo();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN || newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mapFragment.deselectPath();
                    pathInfo.resetPathInfo();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        drawerLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                String username = user.getUsername();
                username = (username != null && !username.equals("")) ? username : user.getEmail();
                ((TextView) findViewById(R.id.txtUsername)).setText(username);
                ((TextView) findViewById(R.id.txtFullName)).setText(user.getFullName());
            }
        });
        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);
        View imgAccount = navigationView.getHeaderView(0).findViewById(R.id.imgAccount);
        imgAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, AccountActivity.class));
            }
        });
        findViewById(R.id.btnMapType).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(activity, view);
                popupMenu.inflate(R.menu.map_type_menu);
                popupMenu.setOnMenuItemClickListener(MainActivity.this);
                popupMenu.show();
            }
        });
        Settings.load(this);
        TrackingService.screenOrientation = getResources().getConfiguration().orientation;
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        MapApp mapApp = ((MapApp) getApplication());
        //Load image info from gallery
        Bundle imageInfo = getIntent().getBundleExtra("imageInfo");
        /*
            DAO - Data Access Object = local database
            Preferences = localStorage
            Bundle = memory
         */
        DaoSession daoSession = mapApp.getDaoSession();
        pathDao = daoSession.getPathDao();
        UserDao userDao = daoSession.getUserDao();
        pref = Utils.getSharedPreferences(this);
        //Load user
        user = User.loadUser(pref, userDao);
        if (user == null) {
            finish();
            startActivity(new Intent(activity, SignInActivity.class));
        }
        //Load Paths
        PathsData.load(pathDao);
        //Load map state
        mapState = MapState.load(pref);
        //Overwrite marker if image info is exists
        if (imageInfo != null) {
            String imagePath = imageInfo.getString("imagePath");
            double[] latLong = imageInfo.getDoubleArray("imageLatLong");
            File file = new File(imagePath);
            if (file.exists()) {
                mapState.markerImagePath = imagePath;
                mapState.markerPosition = new Point(latLong[0], latLong[1]);
                mapState.cameraTarget = new Point(latLong[0], latLong[1]);
            } else {
                mapState.markerImagePath = null;
            }
        }
        mapState.selPathId = getIntent().getLongExtra("pathId", 0);
        setIntent(new Intent());
        setMapFragment();
        fragmentManager.beginTransaction()
                .add(R.id.mapLayout, mapFragment)
                .commit();
        mapFragmentListener = new MapFragment.MapFragmentListener() {
            @Override
            public void onPathSelect(long pathId) {
                pathInfo.viewPathInfo(pathId);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            @Override
            public void onPathDeselect() {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        };
        mapFragment.setListener(mapFragmentListener);
        //Check all permissions
        if (!isPermitted())
            return;
        Utils.setLocationSettings(activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapState.save(pref);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        showHidePaths = menu.findItem(R.id.showHidePaths);
        showHideImages = menu.findItem(R.id.showHideImages);
        changeMap = menu.findItem(R.id.changeMap);
        if (mapState.isPathsShown)
            showHidePaths.setTitle(R.string.hide_all_paths);
        if (mapState.isImagesShown)
            showHideImages.setTitle(R.string.hide_all_images);
        if (TrackingService.isServiceRunning)
            showHidePaths.setEnabled(false);
        if (mapState.isMapBox)
            changeMap.setTitle(R.string.switch_to_google_map);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Toggle file_paths and images and the display of the menu
        switch (item.getItemId()) {
            case R.id.showHidePaths:
                mapState.isPathsShown = !mapState.isPathsShown;
                if (mapState.isPathsShown) {
                    mapFragment.showAllPaths();
                    showHidePaths.setTitle(R.string.hide_all_paths);
                } else {
                    mapFragment.hideAllPaths();
                    showHidePaths.setTitle(R.string.show_all_paths);
                }
                break;
            case R.id.showHideImages:
                mapState.isImagesShown = !mapState.isImagesShown;
                if (mapState.isImagesShown) {
                    mapFragment.showAllImages();
                    showHideImages.setTitle(R.string.hide_all_images);
                } else {
                    mapFragment.hideAllImages();
                    showHideImages.setTitle(R.string.show_all_images);
                }
                break;
            case R.id.changeMap:
                mapState.isMapBox = !mapState.isMapBox;
                if (mapState.isMapBox)
                    changeMap.setTitle(R.string.switch_to_google_map);
                else
                    changeMap.setTitle(R.string.switch_to_mapbox);
                setMapFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.mapLayout, mapFragment)
                        .commit();
                mapFragment.setListener(mapFragmentListener);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        TrackingService.screenOrientation = newConfig.orientation;
    }

    private void setMapFragment() {
        if (mapState.isMapBox) {
            mapFragment = new MapBoxFragment();
            mapFragment1 = new GoogleMapFragment();
        } else {
            mapFragment = new GoogleMapFragment();
            mapFragment1 = new MapBoxFragment();
        }
    }

    private boolean isPermitted() {
        String[] permissions = new String[]{
                permission.ACCESS_FINE_LOCATION,
                permission.ACCESS_COARSE_LOCATION,
                permission.CAMERA,
                permission.WRITE_EXTERNAL_STORAGE,
                permission.READ_EXTERNAL_STORAGE
        };
        if (!Utils.checkPermissions(this, permissions)) {
            try {
                ActivityCompat.requestPermissions(this, permissions, 1);
            } catch (Exception ignored) {
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0) {
            boolean allGranted = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED)
                    allGranted = false;
            }
            if (allGranted) {
                Utils.setLocationSettings(this);
            } else {
                Utils.toast(this, "Not all permissions are granted. Closing the application.");
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Utils.REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                recreate();
            } else if (resultCode == RESULT_CANCELED) {
                Utils.toast(this, "The location settings is not checked. Closing the application.");
                finish();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void onTrackClick(final boolean isChecked) {
        Utils.setLocationSettings(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if (isChecked) {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    TrackingService.getLastKnownLocation(locationProviderClient, new Utils.LocationResultCallback() {
                        @Override
                        void onLocationResult(Location location) {
                            //TODO
                            Utils.toast(activity, "Move with your device to track your path");
                            startService(new Intent(activity, TrackingService.class));
                            /*
                            if (location.getAccuracy() < 10) {
                                Utils.toast(activity, "Move with your device to track your path");
                                startService(new Intent(activity, TrackingService.class));
                            } else {
                                Utils.toast(activity, "Low GPS signal. Your location is not accurate.");
                                btnTrack.setChecked(false);
                            }
                            */
                        }
                    });
                } else {
                    endTracking();
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnTrackLocation(Location location) {
        mapFragment.trackPath(location);
    }

    public void endTracking() {
        List<Point> pathPoints = TrackingService.getPathPoints();
        stopService(new Intent(this, TrackingService.class));
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        showHidePaths.setEnabled(true);
        mapFragment.endTrack();
        if (pathPoints.size() > 1) {
            long pathNo = pathDao.count() + 1;
            String pathName = "Path " + pathNo;
            PathMenuFragment pathMenuFragment = PathMenuFragment.newInstance(pathName);
            pathMenuFragment.show(fragmentManager, pathMenuFragment.getTag());
            pathMenuFragment.setListener(new PathMenuFragment.PathMenuListener() {
                @Override
                public void onSavePathClicked(String pathName) {
                    mapFragment.savePath(pathDao, user.getId(), pathName);
                    Utils.toast(activity, "Path saved");
                }

                @Override
                public void onDeletePathClicked() {
                    mapFragment.deletePath();
                }
            });
        }
    }

    public void onMenuButtonClick(View v) {
        drawerLayout.openDrawer(Gravity.START);
    }

    public void onMyLocationClick(View v) {
        getMyLocation();
    }

    @SuppressLint("MissingPermission")
    public void getMyLocation() {
        TrackingService.getLastKnownLocation(locationProviderClient, new Utils.LocationResultCallback() {
            @Override
            void onLocationResult(Location location) {
                Utils.toast(activity, "Location accuracy: " + location.getAccuracy());
                mapFragment.setLocation(location.getLatitude(), location.getLongitude());
            }
        });
    }

    public void onSaveMapClicked(View v) {
        startActivity(new Intent(this, MapSelectorActivity.class));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.offlineMaps:
                startActivity(new Intent(this, MapsActivity.class));
                return true;
            case R.id.savedPaths:
                startActivity(new Intent(this, PathsActivity.class));
                return true;
            case R.id.camera:
                startActivity(new Intent(this, CameraActivity.class));
                return true;
            case R.id.gallery:
                startActivity(new Intent(this, GalleryActivity.class));
                return true;
            case R.id.storageManager:
                startActivity(new Intent(this, StorageActivity.class));
                return true;
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.logOut:
                logOut();
                return true;
        }
        return false;
    }

    public void logOut() {
        stopService(new Intent(this, TrackingService.class));
        User.logOut(pref);
        finish();
        startActivity(new Intent(this, SignInActivity.class));
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int mapType = MapState.TYPE_NORMAL;
        switch (item.getItemId()) {
            case R.id.satellite:
                mapType = MapState.TYPE_SATELLITE;
                break;
            case R.id.hybrid:
                mapType = MapState.TYPE_HYBRID;
                break;
        }
        mapFragment.setType(mapType);
        return true;
    }
}
