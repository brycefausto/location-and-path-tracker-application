package com.myapps.mapproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AccountActivity extends AppCompatActivity {

    private final Context context = this;

    private TextView txtFullName, txtUsername, txtEmail;
    private User user;
    private UserDao userDao;
    private PathDao pathDao;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        txtFullName = findViewById(R.id.txtFullName);
        txtUsername = findViewById(R.id.txtUsername);
        txtEmail = findViewById(R.id.txtEmail);
        DaoSession daoSession = ((MapApp) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();
        pathDao = daoSession.getPathDao();
        pref = getSharedPreferences("pref", MODE_PRIVATE);
        user = User.loadUser(pref, userDao);
        showAccountInfo();
    }

    private void showAccountInfo() {
        txtFullName.setText(user.getFullName());
        txtUsername.setText(user.getUsername());
        txtEmail.setText(user.getEmail());
    }

    public void onEditAccountClick(View v) {
        View view = getLayoutInflater().inflate(R.layout.edit_account_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        final EditText editFullName, editUsername, editPassword, editNewPassword, editRetypePassword;
        editFullName = view.findViewById(R.id.editFullName);
        editUsername = view.findViewById(R.id.editUsername);
        editPassword = view.findViewById(R.id.editPassword);
        editNewPassword = view.findViewById(R.id.editNewPassword);
        editRetypePassword = view.findViewById(R.id.editRetypePassword);
        final CheckBox chkEditPassword = view.findViewById(R.id.chkEditPassword);
        chkEditPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    editNewPassword.setVisibility(View.VISIBLE);
                    editRetypePassword.setVisibility(View.VISIBLE);
                } else {
                    editNewPassword.setVisibility(View.GONE);
                    editRetypePassword.setVisibility(View.GONE);
                }
            }
        });
        editFullName.setText(user.getFullName());
        editUsername.setText(user.getUsername());
        Utils.showDialog(builder, "Edit Account", null, new Utils.DialogCallback() {

            @Override
            public void onPositiveClick(AlertDialog dialog) {
                String fullName = editFullName.getText().toString();
                String username = editUsername.getText().toString().toLowerCase();
                String password = editPassword.getText().toString();
                String newPassword = editNewPassword.getText().toString();
                String retypePassword = editRetypePassword.getText().toString();
                if (Utils.isValidFullName(context, fullName) && Utils.isValidUsername1(context, username) &&
                        Utils.isValidPassword(context, password)) {
                    if (chkEditPassword.isChecked() && !Utils.isValidPasswords(context, newPassword, retypePassword)) {
                        return;
                    }
                    User user1 = userDao.queryBuilder()
                            .where(UserDao.Properties.Username.eq(username), UserDao.Properties.Id.notEq(user.getId()))
                            .unique();
                    if (user1 == null) {
                        if (password.equals(user.getPassword())) {
                            user.setFullName(fullName);
                            user.setUsername(username);
                            if (chkEditPassword.isChecked()) {
                                user.setPassword(newPassword);
                            }
                            updateUser(user);
                            dialog.dismiss();
                        } else {
                            editPassword.setText("");
                            Utils.toast(context, "Your password is incorrect");
                        }
                    } else {
                        Utils.toast(context, "The username is already existing in accounts");
                    }
                }
            }

            @Override
            public void onNegativeClick(AlertDialog dialog) {
            }
        });
    }

    public void onDeleteAccountClick(View v) {
        View view = getLayoutInflater().inflate(R.layout.delete_account_dialog, null);
        final EditText editPassword = view.findViewById(R.id.editPassword);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        Utils.showDialog(builder, "Delete Account", "Delete your account and user paths?", new Utils.DialogCallback() {
            @Override
            public void onPositiveClick(AlertDialog dialog) {
                String password = editPassword.getText().toString();
                if (Utils.isValidPassword(context, password)) {
                    if (password.equals(user.getPassword())) {
                        deleteUser(user);
                    } else {
                        editPassword.setText("");
                        Utils.toast(context, "Your password is incorrect");
                    }
                }
            }

            @Override
            public void onNegativeClick(AlertDialog dialog) {
            }
        });
    }

    private void updateUser(final User user) {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("userId", user.getId().toString())
                .add("username", user.getUsername())
                .add("password", user.getPassword())
                .add("email", user.getEmail())
                .add("fullName", user.getFullName())
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/update_user.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                Utils.toast(context, "Failed to update account");
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                if (response.isSuccessful() && responseStr.equals("success")) {
                    userDao.update(user);
                    AccountActivity.this.showAccountInfo();
                } else {
                    Utils.toast(context, "Failed to update account");
                }
            }
        });
    }

    private void deleteUser(final User user) {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("userId", user.getId().toString())
                .add("submit", "")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/delete_user.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                Utils.toast(context, "Failed to delete account");
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                if (response.isSuccessful() && responseStr.equals("success")) {
                    pathDao.queryBuilder().where(PathDao.Properties.UserId.eq(user.getId())).buildDelete();
                    userDao.delete(user);
                    User.logOut(pref);
                    finish();
                    Intent intent = new Intent(context, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Utils.toast(context, "Account deleted");
                } else {
                    Utils.toast(context, "Failed to delete account");
                }
            }
        });
    }

}
