package com.myapps.mapproject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressWarnings("ALL")
public class FileService extends Service {

    public final static String ACTION_DOWNLOAD = "DOWNLOAD", ACTION_DELETE = "DELETE";

    public static boolean isRunning;

    private static int notificationId = 1021;

    private final Context context = this;

    private long userId;
    private Handler handler;
    private NotificationManager nManager;
    private PathDao pathDao;

    public FileService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        userId = User.getUserId(pref);
        handler = new Handler(Looper.getMainLooper());
        nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        pathDao = ((MapApp) getApplication()).getDaoSession().getPathDao();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!AppDataService.isRunning) {
            String action = intent.getAction();
            if (action == null) {
                upload();
            } else if (action.equals(ACTION_DOWNLOAD)) {
                download();
            } else if (action.equals(ACTION_DELETE)) {
                delete();
            }
        }
        return START_STICKY;
    }

    private void upload() {
        //Directory for the data files
        final File dir = new File(getExternalFilesDir(null), "data_files");
        if (!dir.exists()) {
            stopSelf();
            return;
        }
        String path1 = dir.getPath() + File.separator + "user_paths.dat";
        String path2 = dir.getPath() + File.separator + "offline_maps.dat";
        final File file1 = new File(path1);
        final File file2 = new File(path2);
        if (!file2.exists()) {
            stopSelf();
            return;
        }
        Notification notification = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setContentTitle("Uploading data files")
                .setContentIntent(createPendingIntent())
                .build();
        nManager.notify(notificationId, notification);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("userId", String.valueOf(userId))
                            .addFormDataPart("files[]", file1.getName(), RequestBody.create(
                                    MediaType.parse("text/plain"), file1))
                            .addFormDataPart("files[]", file2.getName(), RequestBody.create(
                                    MediaType.parse("text/plain"), file2))
                            .build();
                    Request request = new Request.Builder()
                            .url(Server.BASE_URL + "/upload.php")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseStr = response.body().string();
                    if (response.isSuccessful() && responseStr.equals("success")) {
                        Utils.toastOnUI(context, "Files uploaded successfully");
                        EventBus.getDefault().post("File_uploaded");
                    } else {
                        Utils.toastOnUI(context, "Files failed to upload");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.toastOnUI(context, "Files failed to upload");
                }
                stopSelf();
            }
        }).start();
    }

    private void download() {
        //Directory for the data files
        final File dir = new File(getExternalFilesDir(null), "data_files");
        if (!dir.exists()) {
            stopSelf();
            return;
        }
        final String path1 = dir.getPath() + File.separator + "user_paths.dat";
        final String path2 = dir.getPath() + File.separator + "offline_maps.dat";
        final File file1 = new File(path1);
        final File file2 = new File(path2);
        file1.delete();
        file2.delete();
        final Notification notification = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentTitle("Downloading data files")
                .setContentIntent(createPendingIntent())
                .build();
        nManager.notify(notificationId, notification);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean[] isSuccess = new boolean[2];
                    String pathToDownload = Server.BASE_URL + "/uploads/files_" + userId + "/";
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(pathToDownload + file1.getName())
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseStr = response.body().string();
                    if (response.isSuccessful()) {
                        Utils.writeToFile(path1, responseStr);
                        isSuccess[0] = true;
                    } else {
                    }
                    //If the first file is successfully downloaded, continue to the next file
                    if (isSuccess[0]) {
                        request = new Request.Builder()
                                .url(pathToDownload + file2.getName())
                                .build();
                        response = client.newCall(request).execute();
                        responseStr = response.body().string();
                        if (response.isSuccessful()) {
                            Utils.writeToFile(path2, responseStr);
                            isSuccess[1] = true;
                        }
                    }
                    if (isSuccess[0] && isSuccess[1]) {
                        Utils.toastOnUI(context, "Files downloaded successfully");
                        Intent intent = new Intent(getApplicationContext(), AppDataService.class);
                        intent.setAction(AppDataService.ACTION_IMPORT);
                        startService(intent);
                    } else {
                        Utils.toastOnUI(context, "Files failed to download");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                stopSelf();
            }
        }).start();
    }

    private void delete() {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("userId", String.valueOf(userId))
                .add("submit", "submit")
                .build();
        Request request = new Request.Builder()
                .url(Server.BASE_URL + "/delete_files.php")
                .post(body)
                .build();
        client.newCall(request).enqueue(new HttpCallback() {
            @Override
            public void onFailure(IOException e) {
                Utils.toast(context, "Files failed to delete");
            }

            @Override
            public void onResponse(Response response, String responseStr) throws IOException {
                if (response.isSuccessful() && responseStr.equals("success")) {
                    EventBus.getDefault().post("File_deleted");
                } else {
                    Utils.toast(context, "Files failed to delete");
                }
            }
        });
    }

    private PendingIntent createPendingIntent() {
        return PendingIntent.getActivity(this, 0,
                new Intent(this, StorageActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        nManager.cancel(notificationId);
    }

}