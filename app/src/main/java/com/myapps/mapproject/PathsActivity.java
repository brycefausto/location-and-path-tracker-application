package com.myapps.mapproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class PathsActivity extends AppCompatActivity implements ListView.OnItemClickListener {

    private List<Path> paths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paths);
        ListView list = findViewById(R.id.pathList);
        DaoSession daoSession = ((MapApp) getApplication()).getDaoSession();
        UserDao userDao = daoSession.getUserDao();
        User user = User.loadUser(Utils.getSharedPreferences(this), userDao);
        PathDao pathDao = daoSession.getPathDao();
        paths = pathDao.queryBuilder().where(PathDao.Properties.UserId.eq(user.getId())).list();
        PathAdapter adapter = new PathAdapter(this, paths);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Path path = paths.get(i);
        if (path != null) {
            Intent intent = NavUtils.getParentActivityIntent(this);
            intent.putExtra("pathId", path.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            NavUtils.navigateUpTo(this, intent);
        }
    }

    private static class PathAdapter extends BaseAdapter {

        private Context context;
        private List<Path> list;
        private LayoutInflater inflater;

        PathAdapter(Context context, List<Path> list) {
            this.context = context;
            this.list = list;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Path getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder vh;
            if (view == null) {
                view = inflater.inflate(R.layout.path_list_item, viewGroup, false);
                vh = new ViewHolder(view);
                view.setTag(vh);
            } else {
                vh = (ViewHolder) view.getTag();
            }
            Path path = getItem(i);
            String pathIdText = context.getString(R.string.path_id) + String.valueOf(path.getId());
            String pathNameText = context.getString(R.string.path_name) + path.getPathName();
            vh.txtPathID.setText(pathIdText);
            vh.txtPathName.setText(pathNameText);
            return view;
        }

        private static class ViewHolder {
            TextView txtPathID, txtPathName;

            ViewHolder(View v) {
                txtPathID = v.findViewById(R.id.txtPathID);
                txtPathName = v.findViewById(R.id.txtPathName);
            }
        }
    }

}
